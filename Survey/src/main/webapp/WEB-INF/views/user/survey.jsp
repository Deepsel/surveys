<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title> ${survey.topic} </title>
    
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/mystyle.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-collapse collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <a href="${pageContext.request.contextPath}" class="navbar-brand">Сервис социальных опросов</a>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="pages/user/logout"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-md-3">
                <div class="btn-group-vertical" role="group">
                    <a class="btn btn-default" href="${pageContext.request.contextPath}/pages/user/surveys">Доступные Опросы</a>
                    <a class="btn btn-default" href="${pageContext.request.contextPath}/pages/user/history">История</a>
                    <a class="btn btn-default" href="${pageContext.request.contextPath}/pages/user/edit">Сменить пароль</a>
                </div>
            </div>
            <div class="col-md-9">
                <form name="survey" role="form" action="answersurvey" method="post">
                   <h1>${survey.topic}</h1>
                   <hr>
                    <input type="hidden" name="surveyid" value="${survey.id}">

                    <c:forEach items="${survey.questions}" var="question">

                        <div>
                            <h2> ${question.content} </h2>
                            
							<div class="btn-group-vertical" data-toggle="buttons">
                            <c:forEach items="${question.answerOptions}" var="answerOption">

                               <label class="btn btn-default"><input type="radio" name="question${question.id}" value="${answerOption.id}" required> ${answerOption.option} </label>

                            </c:forEach>
                            </div>
                        </div>

                    </c:forEach>
   					<br>
                    <input class="btn btn-success btn-lg marg" type="submit" value="Закончить опрос">
                </form>
                <div class="opps"></div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <span></span>
            <br>
            <p><span class="glyphicon glyphicon-map-marker"></span> Минск, 2017</p>
            <p><span class="glyphicon glyphicon-phone"></span> +375291963227</p>
            <p><span class="glyphicon glyphicon-envelope"></span> deepselr@gmail.com</p>
        </div>
    </footer>

</body>

</html>