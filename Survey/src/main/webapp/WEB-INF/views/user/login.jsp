<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Сервис социальных опросов - Авторизация</title>

    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/mystyle.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/myscript.js"></script>
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-collapse collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <a href="${pageContext.request.contextPath}" class="navbar-brand">Сервис социальных опросов</a>
                </div>
            </div>
        </div>
        <div class="container">


            <form class="form-signin" role="form" action="login" method="post">
                <h2 class="form-signin-heading">Авторизация</h2>
                <c:if test="${error}">
                    <p class="text-danger">Неверный логин или пароль</p>
                </c:if>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" id="email" name="email" type="text" value="${email}">

                    <label>Пароль</label>
                    <input class="form-control" name="password" id="secret" type="password">
                </div>

                <div class="center">
                <input class="btn btn-primary btn-lg" type="submit" onclick="return loginvalid();" value="Войти">
                    </div>  

            </form>
        </div>


        <footer class="footer">
            <div class="container">
                <span></span>
                <br>
                <p><span class="glyphicon glyphicon-map-marker"></span> Минск, 2017</p>
                <p><span class="glyphicon glyphicon-phone"></span> +375291963227</p>
                <p><span class="glyphicon glyphicon-envelope"></span> deepselr@gmail.com</p>
            </div>
        </footer>

    </body>

</html>