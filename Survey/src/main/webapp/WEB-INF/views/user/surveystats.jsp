<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Сервис социальных опросов - Статистика опроса
	${survey.id}</title>

<base href="${pageContext.request.contextPath}/">
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/mystyle.css" rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>
</head>


<body>
	<div class="navbar navbar-inverse navbar-static-top">
		<div class="container">
			<div class="navbar-collapse collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <a
					href="${pageContext.request.contextPath}" class="navbar-brand">Сервис
					социальных опросов</a>
				<ul class="nav navbar-nav navbar-right">
					<c:choose>
						<c:when test="${isAdmin}">
       <li><a href="pages/admin/logout"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li>
						</c:when>
						<c:otherwise>
       <li><a href="pages/user/logout"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li>
						</c:otherwise>
					</c:choose>
					
				</ul>
			</div>
		</div>
	</div>
	<div class="container">

		<div class="row">
			<div class="col-md-3">
                <div class="btn-group-vertical" role="group">
                <c:choose>
						<c:when test="${isAdmin}">
       				<a class="btn btn-default" href="pages/admin/surveys">Опросы</a>
                    <a class="btn btn-default" href="pages/admin/allusers">Пользователи</a>
                    <a class="btn btn-default" href="pages/admin/edit">Сменить пароль</a>
						</c:when>
						<c:otherwise>
       				<a class="btn btn-default" href="pages/user/surveys">Доступные Опросы</a>
                    <a class="btn btn-default" href="pages/user/history">История</a>
                    <a class="btn btn-default" href="pages/user/edit">Сменить пароль</a>
						</c:otherwise>
					</c:choose>
                
                    
                    
                </div>
            </div>
			<div class="col-md-6">
				<h2>${survey.topic}</h2>


				<c:forEach items="${survey.questions}" var="question">

					<div>
						<h3>${question.content}</h3>
						<hr>
						<c:set var="totalAnswers" value="${0}" />
						<c:forEach var="answerOption" items="${question.answerOptions}"
							varStatus="rowCounter1">

							<c:set var="totalAnswers"
								value="${totalAnswers + answerOption.quantity}" />
						</c:forEach>

						<c:forEach items="${question.answerOptions}" var="answerOption">

							<b>${answerOption.option}</b>
							<div class="progress">
								<div class="progress-bar progress-bar-info progress-bar-striped"
									role="progressbar" aria-valuenow="50" aria-valuemin="0"
									aria-valuemax="100"
									style="width:${answerOption.quantity * 100 / totalAnswers}%">

									<fmt:formatNumber var="quantity" minIntegerDigits="2"
										value="${answerOption.quantity * 100 div totalAnswers}" />
									<p>${quantity}%</p>
								</div>
							</div>


						</c:forEach>
					</div>
					<hr class="style5">
				</c:forEach>
				<c:if test="${isAdmin}">
					<a class="btn btn-primary"
						href="pages/admin/editsurvey?id=${survey.id}">Редактировать</a>
					<a class="btn btn-primary"
						href="pages/admin/deletesurvey?id=${survey.id}">Удалить</a>
				</c:if>
				<div class="opps"></div>
			</div>
		</div>
	</div>


	<footer class="footer">
		<div class="container">
			<span></span> <br>
			<p>
				<span class="glyphicon glyphicon-map-marker"></span> Минск, 2017
			</p>
			<p>
				<span class="glyphicon glyphicon-phone"></span> +375291963227
			</p>
			<p>
				<span class="glyphicon glyphicon-envelope"></span>
				deepselr@gmail.com
			</p>
		</div>
	</footer>

</body>

</html>