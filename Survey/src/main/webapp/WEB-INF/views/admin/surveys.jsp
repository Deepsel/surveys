<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Сервис социальных опросов - Опросы</title>

    <base href="${pageContext.request.contextPath}/">
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/css/mystyle.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
</head>


<body>
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-collapse collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <a href="${pageContext.request.contextPath}" class="navbar-brand">Сервис социальных опросов</a>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="pages/admin/logout"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-md-3">
                <div class="btn-group-vertical" role="group">
                    <a class="btn btn-default" href="pages/admin/surveys">Опросы</a>
                    <a class="btn btn-default" href="pages/admin/allusers">Пользователи</a>
                    <a class="btn btn-default" href="pages/admin/edit">Сменить пароль</a>
                    <hr>
                    <a class="btn btn-primary" href="pages/admin/createsurvey">Создать опрос </a>
                </div>
                
            </div>
            <div class="col-md-9">
                <h2>Доступные опросы</h2>
                <c:if test="${empty avaiblesurveys}">
                    Нет доступных опросов.
                </c:if>

                <c:forEach items="${avaiblesurveys}" var="survey">
                    <div class="jumbotron">
                        <h3><a href="pages/admin/surveystats?id=${survey.id}">${survey.topic}</a></h3>
                        <hr>
                        <small> ${survey.description} </small>
                    </div>


                </c:forEach>
                <div class="opps"></div>
            </div>
        </div>
    </div>


    <footer class="footer">
        <div class="container">
            <span></span>
            <br>
            <p><span class="glyphicon glyphicon-map-marker"></span> Минск, 2017</p>
            <p><span class="glyphicon glyphicon-phone"></span> +375291963227</p>
            <p><span class="glyphicon glyphicon-envelope"></span> deepselr@gmail.com</p>
        </div>
    </footer>

</body>

</html>