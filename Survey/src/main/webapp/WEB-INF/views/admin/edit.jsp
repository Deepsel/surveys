<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Администратор - Сменить пароль</title>

    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/mystyle.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-collapse collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <a href="${pageContext.request.contextPath}" class="navbar-brand">Сервис социальных опросов</a>
                <ul class="nav navbar-nav navbar-right">
       <li><a href="${pageContext.request.contextPath}/pages/admin/logout"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="btn-group-vertical" role="group">
						
       				<a class="btn btn-default" href="${pageContext.request.contextPath}/pages/admin/surveys">Опросы</a>
                            <a class="btn btn-default" href="${pageContext.request.contextPath}/pages/admin/allusers">Пользователи</a>
                            <a class="btn btn-default" href="${pageContext.request.contextPath}/pages/admin/edit">Сменить пароль</a>
				
                </div>
            </div>
            <div class="col-md-6">
                <form class="form-edit" role="form" action="edit" method="post">
                    <h2 class="form-signin-heading">Сменить пароль</h2>
                    <c:if test="${error}">
                        <p class="text-danger">Произошла ошибка.</p>
                    </c:if>
                    <div class="form-group">
                        <label>Старый пароль</label>
                        <input class="form-control" name="oldpassword" type="password" required>

                        <label>Новый пароль</label>
                        <input class="form-control" name="newpassword" type="password" required>
                    </div>
                    <div class="center">
                        <input class="btn btn-primary" type="submit" value="Сменить пароль">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <span></span>
            <br>
            <p><span class="glyphicon glyphicon-map-marker"></span> Минск, 2017</p>
            <p><span class="glyphicon glyphicon-phone"></span> +375291963227</p>
            <p><span class="glyphicon glyphicon-envelope"></span> deepselr@gmail.com</p>
        </div>
    </footer>


</body>

</html>