<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Сервис социальных опросов - Создать опрос</title>

    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/mystyle.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/scriptforservicesurvey.js"></script>

</head>

<body>
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-collapse collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <a href="${pageContext.request.contextPath}" class="navbar-brand">Сервис социальных опросов</a>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="${pageContext.request.contextPath}/pages/admin/logout"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="btn-group-vertical" role="group">
                    <a class="btn btn-default" href="${pageContext.request.contextPath}/pages/admin/surveys">Опросы</a>
                    <a class="btn btn-default" href="${pageContext.request.contextPath}/pages/admin/allusers">Пользователи</a>
                    <a class="btn btn-default" href="${pageContext.request.contextPath}/pages/admin/edit">Сменить пароль</a>
                </div>
            </div>
            <div class="col-md-9">
                <div class="control-group" id="fields">
                    <div class="controls">
                        <form action="createsurvey" method="post" role="form" autocomplete="off">
                            <div class="form-group">
                               <br>
                                <label for="comment">Тема</label>
                                <input class="form-control" type="text" name="topic" required />
                                <label for="comment">Описание</label>
                                <textarea class="form-control" rows="5" name="desc" required></textarea>
                            </div>
                            <div class="input_fields_wrap">
                                <label>Вопрос</label>
                                <div class="entry input-group col-xs-8"><input class="form-control" type="text" name="question1" required></div>
                                <label>Варианты ответа</label>
                                <div class="entry input-group col-xs-2">
                                    <input class="form-control" type="text" name="option1" required/>
                                    <input class="form-control" type="text" name="option1" required/>
                                    <input class="form-control" type="text" name="option1" />
                                    <input class="form-control" type="text" name="option1" />
                                </div>

                            </div>
                            <div>
                                <br>
                                <button type="button" class="btn btn-success">Добавить вопрос</button>
                                <input class="btn btn-default" type="submit" value="Создать">
                            </div>

                            <div class="opps"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <span></span>
            <br>
            <p><span class="glyphicon glyphicon-map-marker"></span> Минск, 2017</p>
            <p><span class="glyphicon glyphicon-phone"></span> +375291963227</p>
            <p><span class="glyphicon glyphicon-envelope"></span> deepselr@gmail.com</p>
        </div>
    </footer>
</body>

</html>