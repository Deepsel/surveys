<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Сервис социальных опросов - Пользователи</title>

    <base href="${pageContext.request.contextPath}/">
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/css/mystyle.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
</head>

<body>
   <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-collapse collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <a href="${pageContext.request.contextPath}" class="navbar-brand">Сервис социальных опросов</a>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="pages/admin/logout"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
    <div class="col-md-3">
                <div class="btn-group-vertical" role="group">
                    <a class="btn btn-default" href="pages/admin/surveys">Опросы</a>
                    <a class="btn btn-default" href="pages/admin/allusers">Пользователи</a>
                    <a class="btn btn-default" href="pages/admin/edit">Сменить пароль</a>
                </div>
            </div>
            <div class="col-md-9">
    <h2> Пользователи </h2>
    <table class="table table-striped table-bordered">
        <tbody>
            <tr>
                <td><b>Id</b></td>
                <td><b>Email</b></td>
                <td><b>Роль</b></td>
                <td><b>Пол</b></td>
                <td><b>Возраст</b></td>
                <td><b>Фамилия</b></td>
                <td><b>Имя</b></td>
                <td><b>Отчество</b></td>
            </tr>
            <c:forEach items="${users}" var="user">
                <tr>
                    <td>${user.id}</td>
                    <td>${user.email}</td>
                    <td>
                    <c:if test = "${user.role == 'ADMIN'}">Администратор</c:if>
                    <c:if test = "${user.role == 'PARTICIPANT'}">Пользователь</c:if>
                    </td>
                    <td>
                    <c:if test = "${user.sex == 'FEMALE'}">Женский</c:if>
                    <c:if test = "${user.sex == 'MALE'}">Мужской</c:if>
                    </td>
                    <td style="text-align:center;">${user.age}</td>
                    <td>${user.firstName}</td>
                    <td>${user.username}</td>
                    <td>${user.lastName}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    </div>
</div>
<footer class="footer">
        <div class="container">
            <span></span>
            <br>
            <p><span class="glyphicon glyphicon-map-marker"></span> Минск, 2017</p>
            <p><span class="glyphicon glyphicon-phone"></span> +375291963227</p>
            <p><span class="glyphicon glyphicon-envelope"></span> deepselr@gmail.com</p>
        </div>
    </footer>

</body>

</html>