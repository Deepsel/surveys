function del(x) {
    'use strict';
    var selector = ".deleteButton" + x;
    document.querySelector(selector).click();
}
var x = 1;
var y = 1;

$(document).ready(function () {
    var max_fields = 10;
    var wrapper = $(".input_fields_wrap");
    var add_button = $(".btn-success");


    $(add_button).click(function (e) {
        e.preventDefault();
        if (y < max_fields) {
            x++;
            y++;
            $(wrapper).append('<div><label>Вопрос</label>' + '<div class="entry input-group col-xs-10"><input class="form-control" type="text" name="question' + x + '"/><span class="input-group-btn"><a class="btn btn-danger" onclick="del(' + x + ');">Удалить вопрос</a></span></div>' + '<label>Варианты ответа</label>' +
                '<div class="entry input-group col-xs-2"><input class="form-control" type="text" name="option' + x + '"/>' +
                '<input class="form-control" type="text" name="option' + x + '"/>' +
                '<input class="form-control" type="text" name="option' + x + '"/>' +
                '<input class="form-control" type="text" name="option' + x + '"/></div><a class="deleteButton' + x + '" onclick="cldel(' + x + ');" href="#"></a></div>');
        }
    });
  
});

function cldel(x) {
    'use strict';
    $(".input_fields_wrap").on("click", ".deleteButton" + x, function (e) {
        e.preventDefault();
        $(this).parent('div').remove();
        y--;
    });

}