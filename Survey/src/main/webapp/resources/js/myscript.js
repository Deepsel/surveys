function firstnamevalidate() {
    'use strict';
    var valid;
    if (/\S/.test(document.getElementById('surname').value) && document.getElementById('surname').value.length !== 0) {
        document.getElementById('sur').className = 'glyphicon glyphicon-ok';
        valid = true;
    } else {
        document.getElementById('sur').className = 'glyphicon glyphicon-remove';
        valid = false;
    }
    return valid;
}

function usernamevalidate() {
    'use strict';
    var valid;
    if (/\S/.test(document.getElementById('name').value) && document.getElementById('name').value.length !== 0) {
        document.getElementById('namer').className = 'glyphicon glyphicon-ok';
        valid = true;
    } else {
        document.getElementById('namer').className = 'glyphicon glyphicon-remove';
        valid = false;
    }
    return valid;
}

function emailvalidate() {
    'use strict';
    var valid = false,
        r = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
    if (r.test(document.getElementById('email').value) && document.getElementById('email').value.length !== 0) {
        document.getElementById('emailer').className = 'glyphicon glyphicon-ok';
        valid = true;
    } else {
        document.getElementById('emailer').className = 'glyphicon glyphicon-remove';
        valid = false;
    }
    return valid;
}

function passwordvalidate() {
    'use strict';
    var valid;
    if (/\S/.test(document.getElementById('secret').value) && document.getElementById('secret').value.length !== 0) {
        document.getElementById('secreter').className = 'glyphicon glyphicon-ok';
        valid = true;
    } else {
        document.getElementById('secreter').className = 'glyphicon glyphicon-remove';
        valid = false;
    }
    return valid;
}

function agevalidate() {
    'use strict';
    var valid;
    if (/\d/.test(document.getElementById('age').value) && document.getElementById('age').value.length !== 0) {
        document.getElementById('ager').className = 'glyphicon glyphicon-ok';
        valid = true;
    } else {
        document.getElementById('ager').className = 'glyphicon glyphicon-remove';
        valid = false;
    }
    return valid;
}

function sexvalidate() {
    'use strict';
    var valid;
    if (document.getElementById('male').checked === true || document.getElementById('female').checked === true) {
        document.getElementById('sex').className = 'glyphicon glyphicon-ok';
        valid = true;
    } else {
        document.getElementById('sex').className = 'glyphicon glyphicon-remove';
        valid = false;
    }
    return valid;
}

function validate() {
    'use strict';
    var arr = [];
    var valid = true;
    arr.push(firstnamevalidate());
    arr.push(usernamevalidate());
    arr.push(emailvalidate());
    arr.push(passwordvalidate());
    arr.push(agevalidate());
    arr.push(sexvalidate());
    var i;
    for (i = 0; i < arr.length; i++) {
        if (arr[i] === false) {
            valid = false;
            break;
        }
    }
    return valid;
}

function emailvalid() {
    'use strict';
    var valid = false,
        r = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
    if (r.test(document.getElementById('email').value) && document.getElementById('email').value.length !== 0) {
        document.getElementById('email').className = 'form-control';
        valid = true;
    } else {
        document.getElementById('email').className = 'form-control-invalid';
        valid = false;
    }
    return valid;
}

function passwordvalid() {
    'use strict';
    var valid;
    if (/\S/.test(document.getElementById('secret').value) && document.getElementById('secret').value.length !== 0) {
        document.getElementById('secret').className = 'form-control';
        valid = true;
    } else {
        document.getElementById('secret').className = 'form-control-invalid';
        valid = false;
    }
    return valid;
}

function loginvalid() {
    'use strict';
    var arr = [];
    var valid = true;
    arr.push(emailvalid());
    arr.push(passwordvalid());
    var i;
    for (i = 0; i < arr.length; i++) {
        if (arr[i] === false) {
            valid = false;
            break;
        }
    }
    return valid;
}