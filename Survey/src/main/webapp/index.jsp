<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Сервис социальных опросов</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/mystyle.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

</head>

<body>

    <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-collapse collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <a href="${pageContext.request.contextPath}" class="navbar-brand">Сервис социальных опросов</a>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="pages/signup"><span class="glyphicon glyphicon-user"></span> Регистрация</a></li>
                    <li><a href="pages/login"><span class="glyphicon glyphicon-log-in"></span> Войти</a></li>
                </ul>
            </div>

        </div>
    </div>
    <div id="carousel" class="carousel slide">
        <div class="carousel-inner">
            <div class="item active">
                <img src="${pageContext.request.contextPath}/resources/images/bg.jpg" alt="">
                <div class="carousel-caption">
                    <h1>Добро пожаловать!</h1>
                    <h3>Популярный инструмент для исследования удовлетворенности пользователей, получения отзывов, маркетингового исследования и другие онлайн опросники и анкеты.
                    </h3>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
      <div class="container">
        <span></span>
            <br>
            <p><span class="glyphicon glyphicon-map-marker"></span> Минск, 2017</p>
            <p><span class="glyphicon glyphicon-phone"></span> +375291963227</p>
            <p><span class="glyphicon glyphicon-envelope"></span> deepselr@gmail.com</p>
      </div>
    </footer>

</body>

</html>