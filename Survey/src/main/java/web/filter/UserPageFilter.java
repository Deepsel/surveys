package web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.User;
import entity.User.Role;


@WebFilter("/pages/user/*")
public class UserPageFilter implements Filter  {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse resp = (HttpServletResponse)response;
		User user = (User) req.getSession().getAttribute("authenticated");
		if(user == null) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		} else {
			Role role = user.getRole();
			if(!Role.PARTICIPANT.equals(role)){
				resp.sendError(HttpServletResponse.SC_NOT_FOUND);
				return;
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		
		
	}

}
