package web.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import entity.Survey;
import entity.User;

public class SurveyHistoryAction extends SurveyAction {

	@Override
	public String execute(HttpServletRequest request) {
		String view = "user/history";
		User user = (User) request.getSession().getAttribute("authenticated");
		List<Survey> surveys = surveyService.passed(user.getId());
		request.setAttribute("surveys", surveys);
		return view;
	}

}
