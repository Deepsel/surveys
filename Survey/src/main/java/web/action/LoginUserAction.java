package web.action;

import javax.servlet.http.HttpServletRequest;

import entity.User;
import entity.User.Role;
import web.util.validation.LoginFormValidation;

public class LoginUserAction extends UserAction {

	@Override
	public String execute(HttpServletRequest request) {
		String method = request.getMethod();

		if (method.equals("POST")) {
			return post(request);
		} else {
			return get(request);
		}
	}

	private String get(HttpServletRequest request) {
		// request.setAttribute("form", new UserCreateForm());

		return "user/login";
	}

	private String post(HttpServletRequest request) {

		String view = "user/login";
		LoginFormValidation validator = new LoginFormValidation();
		String email = request.getParameter("email");
		String password = request.getParameter("password");

		if (validator.check(email, password)) {
			User user = new User();
			user = userService.authenticate(request.getParameter("email"), request.getParameter("password"));
			if (!"".equals(user.getPassword())) { // is User not Anonym
				request.getSession().setAttribute("authenticated", user);
				if (Role.ADMIN.equals(user.getRole())) {
					view = "redirect:/pages/admin/detail?id=" + user.getId();
				} else {
					view = "redirect:/pages/user/detail?id=" + user.getId();
				}
			} else {
				request.setAttribute("error", "true");
			}
		}
		request.setAttribute("error", "true");
		request.setAttribute("email", email);

		return view;
	}

}
