package web.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import entity.AnswerOption;
import entity.Question;
import entity.Survey;

public class EditSurveyAction extends SurveyAction {

	@Override
	public String execute(HttpServletRequest request) {
		String method = request.getMethod();

		if (method.equals("POST")) {
			return post(request);
		} else {
			return get(request);
		}

	}

	private String get(HttpServletRequest request) {

		long id = Long.parseLong((request.getParameter("id")));
		Survey survey = surveyService.get(id);
		request.setAttribute("survey", survey);
		String view = "admin/editsurvey";
		return view;
	}

	private String post(HttpServletRequest request) {
		String view = "redirect:/pages/admin/surveys";
		long id = Long.parseLong(request.getParameter("surveyid"));
		Survey survey = surveyService.get(id);
		survey.setTopic(request.getParameter("topic"));
		survey.setDescription(request.getParameter("desc"));
		List<Question> questions = new ArrayList<Question>();
		for (int i = 0; i <= 100; i++) {
			if (!(request.getParameter("question" + i) == null) && !("".equals(request.getParameter("question" + i)))) {
				Question question = new Question();
				question.setQuestion(request.getParameter("question" + i));
				List<AnswerOption> answerOptions = new ArrayList<AnswerOption>();
				for (int j = 0; j < request.getParameterValues("option" + i).length; j++) {
					AnswerOption answerOption = new AnswerOption();
					if (!("".equals(request.getParameterValues("option" + i)[j]))) {
						answerOption.setOption(request.getParameterValues("option" + i)[j]);
						answerOptions.add(answerOption);
					}
				}
				question.setAnswerOptions(answerOptions);
				questions.add(question);
			}
		}
		if(questions.isEmpty()) {
			request.setAttribute("Error", "error");
		} else {
		survey.setQuestions(questions);
		surveyService.update(survey);
		}
		return view;
	}
}
