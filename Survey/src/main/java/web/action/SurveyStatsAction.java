package web.action;

import javax.servlet.http.HttpServletRequest;

import entity.Survey;
import entity.User;
import entity.User.Role;

public class SurveyStatsAction extends SurveyAction {

	@Override
	public String execute(HttpServletRequest request) {
		long id = Long.parseLong(request.getParameter("id"));
		User user = (User) request.getSession().getAttribute("authenticated");
		Survey survey = surveyService.get(id);
		request.setAttribute("survey", survey);
		request.setAttribute("isAdmin", Role.ADMIN.equals(user.getRole()));
		return "user/surveystats";
	}

}
