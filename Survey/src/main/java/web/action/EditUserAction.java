package web.action;

import javax.servlet.http.HttpServletRequest;

import entity.User;
import entity.User.Role;
import web.util.validation.ChangePassValidation;

public class EditUserAction extends UserAction{

	@Override
	public String execute(HttpServletRequest request) {
		String method = request.getMethod();

		if (method.equals("POST")) {
			return post(request);
		} else {
			return get(request);
		}
	}

	private String get(HttpServletRequest request) {
		String view = "";
		User user =  (User) request.getSession().getAttribute("authenticated");
		if(Role.PARTICIPANT.equals(user.getRole())) {
			request.setAttribute("isUser", Role.PARTICIPANT.equals(user.getRole()));
			view = "user/edit";
		} else {
			request.setAttribute("isAdmin", Role.ADMIN.equals(user.getRole()));
		    view = "admin/edit";
		}
		
		return view;
	}

	private String post(HttpServletRequest request) {
		String view = "";
		User user =  (User) request.getSession().getAttribute("authenticated");
		ChangePassValidation validator = new ChangePassValidation();
		String oldpass = request.getParameter("oldpassword");
		String newpass = request.getParameter("newpassword");
		if(validator.check(oldpass, newpass) && oldpass.equals(user.getPassword())) {
			userService.changePassword(user, newpass);
			if(Role.PARTICIPANT.equals(user.getRole())) {
			  view = "redirect:/pages/user/edit"; 
			  request.setAttribute("isUser", Role.PARTICIPANT.equals(user.getRole()));
			}else {
			    view = "redirect:/pages/admin/edit";
			    request.setAttribute("isAdmin", Role.ADMIN.equals(user.getRole()));
			}
		} else {
			if(Role.PARTICIPANT.equals(user.getRole())) {
				  view = "user/edit"; 
				  request.setAttribute("isUser", Role.PARTICIPANT.equals(user.getRole()));
				}else {
				    view = "admin/edit";
				    request.setAttribute("isAdmin", Role.ADMIN.equals(user.getRole()));
				}
			request.setAttribute("error", "true");
		}
		return view;
	}

}
