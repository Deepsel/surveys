package web.action;

import javax.servlet.http.HttpServletRequest;

import dao.exception.DaoException;
import entity.User;
import entity.User.Role;
import entity.User.Sex;
import web.util.validation.SignupFormValidation;

public class CreateUserAction extends UserAction {

	public String execute(HttpServletRequest request) {
		String method = request.getMethod();

		if (method.equals("POST")) {
			return post(request);
		} else {
			return get(request);
		}
	}

	private String get(HttpServletRequest request) {
		return "user/signup";
	}

	private String post(HttpServletRequest request) {

		String view = "user/signup";
		SignupFormValidation validator = new SignupFormValidation(request);
		User user = new User();

		Role role = Role.PARTICIPANT;
		Sex sex = Sex.valueOf(request.getParameter("sex"));
		user.setUsername(request.getParameter("username"));
		user.setPassword(request.getParameter("password"));
		user.setEmail(request.getParameter("email"));
		user.setRole(role);
		user.setSex(sex);
		user.setAge(Integer.parseInt(request.getParameter("age")));
		user.setFirstName(request.getParameter("firstname"));
		user.setLastName(request.getParameter("lastname"));
		if (validator.check()) {
			try {
			user = userService.create(user);
			view = "redirect:/pages/login";
			} catch(DaoException e) {
				view = "user/signup";
				request.setAttribute("form", user);
				request.setAttribute("error", "true");
				
			}
			

		} else {
			view = "user/signup";
			request.setAttribute("form", user);
			request.setAttribute("error", "true");
		}

		return view;
	}
}
