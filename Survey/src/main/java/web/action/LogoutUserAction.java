package web.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LogoutUserAction extends UserAction {

	@Override
	public String execute(HttpServletRequest request) {
		String view =  "redirect:";
		HttpSession session = request.getSession();

		if (!session.isNew()) {
		    session.invalidate();
		    session = request.getSession();
		}
		return view;
	}

}
