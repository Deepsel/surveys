package web.action;

import javax.servlet.http.HttpServletRequest;

public class DetailAdminAction extends UserAction {

	@Override
	public String execute(HttpServletRequest request) {
	String method = request.getMethod();

	if (method.equals("POST")) {
		return post(request);
	} else {
		return get(request);
	}
}

private String get(HttpServletRequest request) {
	// request.setAttribute("form", new UserCreateForm());

	return "admin/detail";
}

private String post(HttpServletRequest request) {


	return "admin/detail";
}

}
