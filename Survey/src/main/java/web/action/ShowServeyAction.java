package web.action;

import javax.servlet.http.HttpServletRequest;

import entity.Survey;

public class ShowServeyAction extends SurveyAction {

	@Override
	public String execute(HttpServletRequest request) {
		String view = "user/survey";
	    long id = Long.parseLong((request.getParameter("id")));
	    Survey survey = surveyService.get(id);
		request.setAttribute("survey", survey);
		return view;
	}

}
