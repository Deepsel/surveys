package web.action;

import service.UserService;
import service.UserServiceImpl;

public abstract class UserAction extends Action {
	
	public static final String PARAMETER_ID = "id";

    protected UserService userService;

    public UserAction() {
    	userService = new UserServiceImpl();
    }

}
