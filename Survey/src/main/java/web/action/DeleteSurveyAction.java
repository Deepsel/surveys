package web.action;

import javax.servlet.http.HttpServletRequest;

import entity.Survey;

public class DeleteSurveyAction extends SurveyAction {

	@Override
	public String execute(HttpServletRequest request) {
		long id = Long.parseLong(request.getParameter("id"));
		String view = "redirect:/pages/admin/surveys";
		Survey survey = surveyService.get(id);
		surveyService.delete(survey);
		return view;
	}

}
