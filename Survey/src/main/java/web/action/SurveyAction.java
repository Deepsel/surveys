package web.action;

import service.SurveyService;
import service.SurveyServiceImpl;

public abstract class SurveyAction extends Action{
	
	protected SurveyService surveyService;

    public SurveyAction() {
    	surveyService = new SurveyServiceImpl();
    }

}
