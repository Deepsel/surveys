package web.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import entity.Question;
import entity.Survey;
import entity.User;

public class AnswerSurveyAction extends SurveyAction {

	@Override
	public String execute(HttpServletRequest request) {
		String method = request.getMethod();

		if (method.equals("POST")) {
			return post(request);
		} else {
			return get(request);
		}
	}

	private String get(HttpServletRequest request) {
		return "user/surveystats";
	}

	private String post(HttpServletRequest request) {
		String view = "user/surveystat";
		long id = Long.parseLong(request.getParameter("surveyid"));
		Survey survey = surveyService.get(id);
		User user = (User) request.getSession().getAttribute("authenticated");
		List<Question> questions = survey.getQuestions();
		List<Long> answers = new ArrayList<Long>();
		for (Question question : questions) {
			answers.add(Long.parseLong(request.getParameter("question" + question.getId())));		
			}
		surveyService.save(survey, user, answers);
		request.setAttribute("survey", survey);
		view = "redirect:/pages/user/surveystats?id=" + survey.getId();
		return view;
	}

}
