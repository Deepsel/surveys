package web.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import entity.Survey;
import entity.User;
import entity.User.Role;

public class AvailableSurveysAction extends SurveyAction {

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("authenticated");
		String view = "";
		if(Role.ADMIN.equals(user.getRole())){
			view = "admin/surveys";
			List<Survey> surveys = surveyService.getAll();
			request.setAttribute("avaiblesurveys", surveys);
		} else {
		
		view  = "user/surveys";
		List<Survey> surveys = surveyService.available(user.getId());
		request.setAttribute("avaiblesurveys", surveys);
		}
		
		return view;		
		
		
	}

	
	

}
