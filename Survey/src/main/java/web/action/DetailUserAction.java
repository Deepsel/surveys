package web.action;

import javax.servlet.http.HttpServletRequest;

public class DetailUserAction extends UserAction {

	@Override
	public String execute(HttpServletRequest request) {
	String method = request.getMethod();

	if (method.equals("POST")) {
		return post(request);
	} else {
		return get(request);
	}
}

private String get(HttpServletRequest request) {
	// request.setAttribute("form", new UserCreateForm());

	return "user/detail";
}

private String post(HttpServletRequest request) {


	return "user/detail";
}

}
