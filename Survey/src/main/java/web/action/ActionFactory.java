package web.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import web.util.RequestMaper;

public class ActionFactory {
	
	private static Map<String, Action> actions;
	
	static {
        actions = new HashMap<>();
        actions.put("pages/signup", new CreateUserAction());
        actions.put("pages/login", new LoginUserAction());
        actions.put("pages/user/detail", new DetailUserAction());
        actions.put("pages/admin/detail", new DetailAdminAction());
        actions.put("pages/user/surveys", new AvailableSurveysAction());
        actions.put("pages/user/survey", new ShowServeyAction());
        actions.put("pages/user/answersurvey", new AnswerSurveyAction());
        actions.put("pages/user/surveystats", new SurveyStatsAction());
        actions.put("pages/user/history", new SurveyHistoryAction());
        actions.put("pages/user/edit", new EditUserAction());
        actions.put("pages/user/logout", new LogoutUserAction());
        actions.put("pages/admin/logout", new LogoutUserAction());
        actions.put("pages/admin/surveys", new AvailableSurveysAction());
        actions.put("pages/admin/surveystats", new SurveyStatsAction());
        actions.put("pages/admin/createsurvey", new CreateSurveyAction());
        actions.put("pages/admin/deletesurvey", new DeleteSurveyAction());
        actions.put("pages/admin/allusers", new AllUserAction());
        actions.put("pages/admin/edit", new EditUserAction());
        actions.put("pages/admin/editsurvey", new EditSurveyAction());
	}
	
	public static Action getAction(HttpServletRequest request) {
		String baseUrl = RequestMaper.getUrl(request);
        
		return actions.get(baseUrl);
    }

}
