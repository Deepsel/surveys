package web.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import entity.User;

public class AllUserAction extends UserAction {

	@Override
	public String execute(HttpServletRequest request) {
		String view = "admin/allusers";
		List<User> users = userService.getAll();
		request.setAttribute("users", users);
		return view;
	}

}
