package web.util.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator implements Validator {

	private static final String EMAIL_PATTERN = "^[\\s]{2,}$";

	private static final Pattern pattern = Pattern.compile(EMAIL_PATTERN);

	private Matcher matcher;

	@Override
	public boolean check(String value) {
		boolean checked = false;

		matcher = pattern.matcher(value);
		if (!matcher.matches()) {
			checked = true;
		}

		return checked;
	}

}
