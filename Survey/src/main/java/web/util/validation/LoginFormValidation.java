package web.util.validation;

public class LoginFormValidation {
	

		public boolean check(String email, String password) {
			boolean checked = false;
			EmailValidator emailValidator = new EmailValidator();
			PasswordValidator passwordValidator = new PasswordValidator();
			
			if (emailValidator.check(email) && passwordValidator.check(password)) {
				checked = true;
			}
			
			
			return checked;
		}
	
	

}
