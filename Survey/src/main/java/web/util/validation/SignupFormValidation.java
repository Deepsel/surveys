package web.util.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

public class SignupFormValidation{

	
private HttpServletRequest request;
	

	public SignupFormValidation (HttpServletRequest request) {
		this.request = request;
	}

	public boolean firstnamevalidate() {
		boolean valid;
		String firstname = request.getParameter("firstname");
		Pattern pattern = Pattern.compile("[\\s]{2,}");
		Matcher matcher = pattern.matcher(firstname);
		if (matcher.find() || firstname.length() == 0) {
			valid = true;
		} else {
			valid = false;
		}
		return !valid;
	}

	public boolean usernamevalidate() {
		boolean valid;
		String username = request.getParameter("username");
		Pattern pattern = Pattern.compile("[\\s]{2,}");
		Matcher matcher = pattern.matcher(username);
		if (matcher.find() || username.length() == 0) {
			valid = true;
		} else {
			valid = false;
		}
		return !valid;
	}

	public boolean emailvalidate() {
		boolean valid;
		EmailValidator validator = new EmailValidator();
		if (request.getParameter("email") != null) {
		valid = validator.check(request.getParameter("email"));
		} else {
			valid = false;
		}
		return valid;
	}
	
	public boolean passwordvalidate() {
		boolean valid;
		PasswordValidator validator = new PasswordValidator();
		if (request.getParameter("password") != null) {
		valid = validator.check(request.getParameter("password"));
		} else {
			valid = false;
		}
		return valid;
	}
	

	public boolean agevalidate() {
		boolean valid = false;
		String age = request.getParameter("age");
		if (age != null && !"".equals(age)) {
			Pattern pattern = Pattern.compile("[\\d]");
			Matcher matcher = pattern.matcher(age);
			if(matcher.find()) {
				valid = true;
			} else {
				valid = false;
			}
		}else {
			valid = false;
		}
		
		return valid;
	}

	public boolean sexvalidate() {
		boolean valid;
		String sex = request.getParameter("sex");
		if ("MALE".equals(sex) || "FEMALE".equals(sex)) {
			valid = true;
		} else {
			valid = false;
		}
		return valid;
	}
	
		

	public boolean check() {
		boolean valid = true;
		boolean[] arr = new boolean[6];
		arr[0] = firstnamevalidate();
		arr[1] = usernamevalidate();
		arr[2] = emailvalidate();
		arr[3] = passwordvalidate();
		arr[4] = agevalidate();
		arr[5] = sexvalidate();
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == false) {
				valid = false;
				break;
			}

		}
		return valid;
	}

}
