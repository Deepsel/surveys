package web.util.validation;

public interface Validator {
	
	boolean check(String value);

}
