package web.util.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator implements Validator {
	
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
	        "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	    private static final Pattern pattern = Pattern.compile(EMAIL_PATTERN);

	    private Matcher matcher;

	@Override
	public boolean check(String value) {
		boolean checked = false;
		
		matcher = pattern.matcher(value);
		if(matcher.matches()) {
			checked = true;
		}
		
		return checked;
	}

}
