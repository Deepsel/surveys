package web.util.validation;

public class ChangePassValidation {
	
	public boolean check(String oldpassword, String newpassword) {
		boolean checked = false;
		PasswordValidator passwordValidator = new PasswordValidator();
		
		if (passwordValidator.check(oldpassword) && passwordValidator.check(newpassword)) {
			checked = true;
		}
		
		
		return checked;
	}


}
