package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import web.action.Action;
import web.action.ActionFactory;

@WebServlet("/pages/*")
public class Controller extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		processRequest(req, resp);
	}

	protected void processRequest(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");

		Action action = ActionFactory.getAction(req);
		String view = action.execute(req);
		
		if (!view.startsWith("redirect:")) {
            req.getRequestDispatcher("/WEB-INF/views/" + view + ".jsp").forward(req, resp);
        } else {
            resp.sendRedirect(req.getContextPath() + view.substring(9));
        }
		
	}

}

