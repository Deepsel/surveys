package dao;

import java.util.List;

import dao.exception.DaoException;
import entity.User;


public interface UserDao {
	User create(User user) throws DaoException;

	User update(User user) throws DaoException;

	void delete(Long user) throws DaoException;

	User get(Long userId) throws DaoException;
	
	User get(String email) throws DaoException;

	List<User> getAll() throws DaoException;

}
