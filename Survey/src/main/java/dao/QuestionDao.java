package dao;

import java.util.List;

import dao.exception.DaoException;
import entity.Question;


public interface QuestionDao {
	
	Question create(Question question) throws DaoException;

	Question update(Question question) throws DaoException;

	void delete(Long questionId) throws DaoException;

	Question get(Long questionId) throws DaoException;
	
	List<Question> getBySurveyId(Long surveyId) throws DaoException;

	List<Question> getAll() throws DaoException;

}
