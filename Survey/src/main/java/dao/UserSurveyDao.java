package dao;

import java.util.List;

import dao.exception.DaoException;
import entity.UserSurvey;

public interface UserSurveyDao {
	
	UserSurvey create(UserSurvey userSurvey) throws DaoException;
	
	List<UserSurvey> getByUserId(Long userId) throws DaoException;

}
