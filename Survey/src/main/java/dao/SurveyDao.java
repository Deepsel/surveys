package dao;

import java.util.List;

import dao.exception.DaoException;
import entity.Survey;

public interface SurveyDao {

	Survey create(Survey survey) throws DaoException;

	Survey update(Survey survey) throws DaoException;

	void delete(Long surveyId) throws DaoException;

	Survey get(Long surveyId) throws DaoException;

	List<Survey> getAll() throws DaoException;
}
