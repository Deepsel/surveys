package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dao.SurveyDao;
import dao.exception.DaoException;
import dao.util.ConnectionPool;
import entity.Survey;

public class SurveyDaoImpl implements SurveyDao {

	private ConnectionPool connectionPool;

	public SurveyDaoImpl() {
		connectionPool = ConnectionPool.getPool();
	}

	@Override
	public Survey create(Survey survey) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet generatedKeys = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement(
					"INSERT INTO `survey`.`survey` (`topic`, `description`) VALUES (?, ?);",
					Statement.RETURN_GENERATED_KEYS);

			statement.setString(1, survey.getTopic());
			statement.setString(2, survey.getDescription());
			statement.execute();
			generatedKeys = statement.getGeneratedKeys();
			if (generatedKeys.next()) {
				survey.setId(generatedKeys.getLong(1));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, generatedKeys);
		}
		return survey;
	}

	@Override
	public Survey update(Survey survey) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement(
					"UPDATE `survey`.`survey` SET `topic`= ?, `description`= ? WHERE `id`=" + survey.getId() + ";");
			statement.setString(1, survey.getTopic());
			statement.setString(2, survey.getDescription());
			statement.execute();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement);
		}
		return survey;
	}

	@Override
	public void delete(Long surveyId) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = (PreparedStatement) connection
					.prepareStatement("DELETE FROM `survey`.`survey` where id = " + surveyId + ";");
			statement.execute();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement);
			;
		}

	}

	@Override
	public Survey get(Long id) throws DaoException {

		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Survey survey = new Survey();

		try {

			statement = (PreparedStatement) connection
					.prepareStatement("SELECT * FROM survey.survey where id =" + id + ";");
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				survey.setId(id);
				survey.setTopic(resultSet.getString("topic"));
				survey.setDescription(resultSet.getString("description"));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, resultSet);
		}

		return survey;
	}

	@Override
	public List<Survey> getAll() throws DaoException {

		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		List<Survey> surveys = new ArrayList<Survey>();

		try {

			statement = (PreparedStatement) connection.prepareStatement("SELECT * FROM survey.survey;");
			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				surveys.add(new Survey((long) resultSet.getInt("id"), resultSet.getString("topic"),
						resultSet.getString("description")));
			}

		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, resultSet);
		}

		return surveys;
	}

}
