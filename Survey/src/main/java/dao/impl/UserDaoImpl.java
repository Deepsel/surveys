package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.UserDao;
import dao.exception.DaoException;
import dao.util.ConnectionPool;
import entity.User;
import entity.User.Role;
import entity.User.Sex;

public class UserDaoImpl implements UserDao {

	private ConnectionPool connectionPool;

	public UserDaoImpl() {
		connectionPool = ConnectionPool.getPool();
	}

	public User create(User user) throws DaoException {
		Connection connection = connectionPool.getConnection();
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement(
					"INSERT INTO `survey`.`user` (`username`, `password`, `email`, `role`, `sex`, `age`, `firstname`, `lastname`) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
			statement.setString(1, user.getUsername());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getEmail());
			statement.setInt(4, user.getRole().ordinal());
			statement.setInt(5, user.getSex().ordinal());
			statement.setInt(6, user.getAge());
			statement.setString(7, user.getFirstName());
			statement.setString(8, user.getLastName());

			statement.execute();

		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement);
		}
		return user;
	}

	public User update(User user) throws DaoException {
		Connection connection = connectionPool.getConnection();
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement(
					"UPDATE `survey`.`user` SET `username`= ?, `password`= ?, `email`= ?, `role`= ?, `sex`= ?, `age`= ?, `firstname`= ?, `lastname`= ? WHERE `id`='"
							+ user.getId() + "';");
			statement.setString(1, user.getUsername());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getEmail());
			statement.setInt(4, user.getRole().ordinal());
			statement.setInt(5, user.getSex().ordinal());
			statement.setInt(6, user.getAge());
			statement.setString(7, user.getFirstName());
			statement.setString(8, user.getLastName());
			statement.execute();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement);
		}

		return user;
	}

	public void delete(Long userId) throws DaoException {
		Connection connection = connectionPool.getConnection();
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection
					.prepareStatement("DELETE FROM `survey`.`user` where id = " + userId + "';");
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement);
		}

	}

	public User get(Long userId) throws DaoException {
		Connection connection = connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;
		User user = new User();

		try {
			statement = (PreparedStatement) connection.createStatement();
			rs = statement.executeQuery("SELECT * FROM `survey`.`user` where id = " + userId + "';");
			if (rs.next()) {
				user.setId(userId);
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setEmail(rs.getString("email"));
				user.setRole(Role.values()[rs.getInt("role")]);
				user.setSex(Sex.values()[rs.getInt("sex")]);
				user.setAge(rs.getInt("age"));
				user.setFirstName(rs.getString("firstname"));
				user.setLastName(rs.getString("lastname"));

			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, rs);
		}

		return user;
	}

	public User get(String email) throws DaoException {
		Connection connection = connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet rs = null;

		User user = new User();

		try {
			statement = (PreparedStatement) connection
					.prepareStatement("SELECT * FROM `survey`.`user` where email = '" + email + "';");
			rs = statement.executeQuery();
			if (rs.next()) {
				user.setId(rs.getLong("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setEmail(email);
				user.setRole(Role.values()[rs.getInt("role")]);
				user.setSex(Sex.values()[rs.getInt("sex")]);
				user.setAge(rs.getInt("age"));
				user.setFirstName(rs.getString("firstname"));
				user.setLastName(rs.getString("lastname"));

			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, rs);
		}
		return user;

	}

	public List<User> getAll() throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		List<User> users = new ArrayList<User>();

		try {

			statement = (PreparedStatement) connection.prepareStatement("SELECT * FROM `survey`.`user`;");
			resultSet = statement.executeQuery();

			while (resultSet.next()) {

				users.add(new User(resultSet.getLong("id"), resultSet.getString("username"),
						resultSet.getString("firstname"), resultSet.getString("lastname"),
						resultSet.getString("password"), resultSet.getString("email"),
						Role.values()[resultSet.getInt("role")], Sex.values()[resultSet.getInt("sex")],
						resultSet.getInt("age")));
			}

		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, resultSet);
		}

		return users;
	}

}
