package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dao.QuestionDao;
import dao.exception.DaoException;
import dao.util.ConnectionPool;
import entity.Question;

public class QuestionDaoImpl implements QuestionDao {

	private ConnectionPool connectionPool;

	public QuestionDaoImpl() {
		connectionPool = ConnectionPool.getPool();
	}

	@Override
	public Question create(Question question) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet generatedKeys = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement(
					"INSERT INTO `survey`.`question` (`content`, `survey_id`) VALUES (?, ?);",
					Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, question.getContent());
			statement.setInt(2, Math.toIntExact(question.getSurvey().getId()));
			statement.execute();
			generatedKeys = statement.getGeneratedKeys();
			if (generatedKeys.next()) {
				question.setId(generatedKeys.getLong(1));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, generatedKeys);
		}
		return question;
	}

	@Override
	public Question update(Question question) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement(
					"UPDATE `survey`.`question` SET `content`= ? WHERE `id`=" + question.getId() + ";");
			statement.setString(1, question.getContent());
			statement.execute();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement);
		}
		return question;

	}

	@Override
	public void delete(Long questionId) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = (PreparedStatement) connection
					.prepareStatement("DELETE FROM `survey`.`question` where id = " + questionId + ";");
			statement.execute();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement);
			;
		}
	}

	@Override
	public Question get(Long questionId) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		Question question = new Question();

		try {
			statement = (PreparedStatement) connection
					.prepareStatement("SELECT * FROM `survey`.`question` where id =" + questionId + ";");
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				question.setId(questionId);
				question.setQuestion(resultSet.getString("content"));
				question.setSurvey(new SurveyDaoImpl().get((long) resultSet.getInt(3)));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, resultSet);
		}
		return question;
	}

	@Override
	public List<Question> getAll() throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		List<Question> questions = new ArrayList<Question>();

		try {

			statement = (PreparedStatement) connection.prepareStatement("SELECT * FROM survey.question;");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {

				questions.add(new Question((long) resultSet.getInt("id"), resultSet.getString("content"),
						new SurveyDaoImpl().get((long) resultSet.getInt("survey_id"))));
			}

		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, resultSet);
		}
		return questions;
	}

	@Override
	public List<Question> getBySurveyId(Long surveyId) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Question> questions = new ArrayList<Question>();
		try {
			statement = (PreparedStatement) connection
					.prepareStatement("SELECT * FROM survey.question where survey_id =" + surveyId + ";");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {

				questions.add(new Question((long) resultSet.getInt("id"), resultSet.getString("content"),
						new SurveyDaoImpl().get((long) resultSet.getInt("survey_id"))));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, resultSet);
		}
		return questions;
	}

}
