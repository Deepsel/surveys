package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import dao.UserSurveyDao;
import dao.exception.DaoException;
import dao.util.ConnectionPool;
import entity.UserSurvey;

public class UserSurveyDaoImpl implements UserSurveyDao {
	
	private ConnectionPool connectionPool;

	public UserSurveyDaoImpl() {
		connectionPool = ConnectionPool.getPool();
	}

	@Override
	public UserSurvey create(UserSurvey userSurvey) throws DaoException {
		
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = (PreparedStatement) connection
					.prepareStatement("INSERT INTO `survey`.`usersurvey` (`user_id`, `survey_id`) VALUES (?, ?);");
			statement.setLong(1, userSurvey.getUserId());
			statement.setLong(2, userSurvey.getSurveyId());
			statement.execute();
			
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement);
		}
		
		return userSurvey;
	}

	@Override
	public List<UserSurvey> getByUserId(Long userId) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<UserSurvey> userSurveys = new ArrayList<UserSurvey>();
		try {
			statement = (PreparedStatement) connection
					.prepareStatement("SELECT * FROM `survey`.`usersurvey` where user_id = " + userId + ";");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				userSurveys.add(new UserSurvey(resultSet.getLong("user_id"), resultSet.getLong("survey_id")));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, resultSet);
		}
		return userSurveys;
	}
	
	

}
