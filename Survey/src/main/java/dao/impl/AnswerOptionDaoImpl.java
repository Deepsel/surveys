package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import dao.AnswerOptionDao;
import dao.exception.DaoException;
import dao.util.ConnectionPool;
import entity.AnswerOption;

public class AnswerOptionDaoImpl implements AnswerOptionDao {

	private ConnectionPool connectionPool;

	public AnswerOptionDaoImpl() {
		connectionPool = ConnectionPool.getPool();
	}

	@Override
	public AnswerOption create(AnswerOption answerOption) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = (PreparedStatement) connection
					.prepareStatement("INSERT INTO `survey`.`answeroption` (`question_id`, `option`) VALUES (?, ?);");
			statement.setLong(1, answerOption.getQuestionId());
			statement.setString(2, answerOption.getOption());
			statement.execute();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement);
		}
		return answerOption;
	}

	@Override
	public AnswerOption update(AnswerOption answerOption) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = (PreparedStatement) connection.prepareStatement(
					"UPDATE `survey`.`answeroption` SET `option`= ? , `quantity`= ? WHERE `id`=" + answerOption.getId() + ";");
			statement.setString(1, answerOption.getOption());
			statement.setLong(2, answerOption.getQuantity());
			statement.execute();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement);
		}
		return answerOption;
	}

	@Override
	public void delete(Long answerOptionId) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = (PreparedStatement) connection
					.prepareStatement("DELETE FROM `survey`.`answeroption` where id = " + answerOptionId + ";");
			statement.execute();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement);
		}

	}

	@Override
	public AnswerOption get(Long answerOptionId) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		AnswerOption answerOption = new AnswerOption();

		try {
			statement = (PreparedStatement) connection
					.prepareStatement("SELECT * FROM `survey`.`answeroption` where id =" + answerOptionId + ";");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				answerOption.setId(answerOptionId);
				answerOption.setQuestionId(resultSet.getLong("question_id"));
				answerOption.setOption(resultSet.getString("option"));
				answerOption.setQuantity(resultSet.getLong("quantity"));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, resultSet);
		}

		return answerOption;
	}

	@Override
	public List<AnswerOption> getByQuestionId(Long questionId) throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<AnswerOption> answerOptions = new ArrayList<AnswerOption>();
		try {
			statement = (PreparedStatement) connection
					.prepareStatement("SELECT * FROM `survey`.`answeroption` where question_id =" + questionId + ";");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {

				answerOptions.add(new AnswerOption((long) resultSet.getInt("id"), (long) resultSet.getInt("question_id"), resultSet.getString("option"), resultSet.getLong("quantity")));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, resultSet);
		}
		return answerOptions;
	}

	@Override
	public List<AnswerOption> getAll() throws DaoException {
		Connection connection = (Connection) connectionPool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<AnswerOption> answerOptions = new ArrayList<AnswerOption>();
		try {
			statement = (PreparedStatement) connection
					.prepareStatement("SELECT * FROM `survey`.`answeroption`;");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {

				answerOptions.add(new AnswerOption((long) resultSet.getInt("id"), (long) resultSet.getInt("question_id"), resultSet.getString("option"), resultSet.getLong("quantity")));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionPool.closeDbResources(connection, statement, resultSet);
		}
		return answerOptions;
	}

}
