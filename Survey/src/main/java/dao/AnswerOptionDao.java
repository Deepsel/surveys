package dao;

import java.util.List;

import dao.exception.DaoException;
import entity.AnswerOption;


public interface AnswerOptionDao {
	
	AnswerOption create(AnswerOption answerOption) throws DaoException;

	AnswerOption update(AnswerOption answerOption) throws DaoException;

	void delete(Long answerOptionId) throws DaoException;

	AnswerOption get(Long answerOptionId) throws DaoException;
	
	List<AnswerOption> getByQuestionId(Long questionId) throws DaoException;

	List<AnswerOption> getAll() throws DaoException;

}
