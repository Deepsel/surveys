package service;

import java.util.List;

import entity.User;
import entity.User.Role;

public interface UserService {
    
    User authenticate(String email, String password);

    void changeRole(User user, Role role);

    void changePassword(User user, String newPassword);

	User create(User user);
	
	List<User> getAll();
}
