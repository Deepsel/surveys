package service;

import java.util.ArrayList;
import java.util.List;

import dao.impl.AnswerOptionDaoImpl;
import dao.impl.QuestionDaoImpl;
import dao.impl.SurveyDaoImpl;
import dao.impl.UserSurveyDaoImpl;
import entity.AnswerOption;
import entity.Question;
import entity.Survey;
import entity.User;
import entity.UserSurvey;

public class SurveyServiceImpl implements SurveyService {

	protected SurveyDaoImpl surveyDao;
	protected QuestionDaoImpl questionDao;
	protected AnswerOptionDaoImpl answerOptionDao;
	protected UserSurveyDaoImpl userSurveyDao;

	public SurveyServiceImpl() {
		surveyDao = new SurveyDaoImpl();
		questionDao = new QuestionDaoImpl();
		answerOptionDao = new AnswerOptionDaoImpl();
		userSurveyDao = new UserSurveyDaoImpl();
	}

	@Override
	public Survey create(Survey survey) {
		Survey addedsurvey = surveyDao.create(survey);
		for (Question question : survey.getQuestions()) {
			question.setSurvey(addedsurvey);
			Question addedquestion = questionDao.create(question);
			for (AnswerOption answerOption : question.getAnswerOptions()) {
				answerOption.setQuestionId(addedquestion.getId());
				answerOptionDao.create(answerOption);
			}
		}

		return survey;
	}

	@Override
	public Survey get(Long surveyId) {
		Survey survey = surveyDao.get(surveyId);
		survey.setQuestions(questionDao.getBySurveyId(surveyId));
		for (Question question : survey.getQuestions()) {
			question.setAnswerOptions(answerOptionDao.getByQuestionId(question.getId()));
		}
		return survey;
	}

	@Override
	public UserSurvey save(Survey survey, User user, List<Long> answers) {

		UserSurvey userSurvey = new UserSurvey(user.getId(), survey.getId());
		userSurveyDao.create(userSurvey);
		for (int i = 0; i < answers.size(); i++) {
			AnswerOption answerOption = answerOptionDao.get(answers.get(i));
			answerOption.setQuantity(answerOption.getQuantity() + 1);
			answerOptionDao.update(answerOption);
		}
		return userSurvey;

	}

	@Override
	public void delete(Survey survey) {
		surveyDao.delete(survey.getId());

	}

	@Override
	public List<Survey> available(Long userId) {
		List<Survey> surveys = surveyDao.getAll();
		List<UserSurvey> userSurveys = userSurveyDao.getByUserId(userId);
		List<Survey> passedSurveys = new ArrayList<Survey>();
		for (UserSurvey userSurvey : userSurveys) {
			passedSurveys.add(surveyDao.get(userSurvey.getSurveyId()));
		}
		surveys.removeAll(passedSurveys);
		return surveys;
	}
	@Override
	public List<Survey> passed(Long userId) {
		
		List<UserSurvey> usersurveys = userSurveyDao.getByUserId(userId);
		List<Survey> surveys = new ArrayList<Survey>();
		for(UserSurvey usersurvey : usersurveys) {
			surveys.add(surveyDao.get(usersurvey.getSurveyId()));
		}
		
		return surveys;
		
	}

	@Override
	public List<Survey> getAll() {
		List<Survey> surveys = surveyDao.getAll();
		return surveys;
	}

	@Override
	public Survey update(Survey survey) {
		surveyDao.update(survey);
		List<Question> questions = questionDao.getBySurveyId(survey.getId());
		for(int i = 0; i < questions.size(); i++) {
			questionDao.delete(questions.get(i).getId());
		}
		for (Question question : survey.getQuestions()) {
			question.setSurvey(survey);
			Question addedquestion = questionDao.create(question);
			for (AnswerOption answerOption : question.getAnswerOptions()) {
				answerOption.setQuestionId(addedquestion.getId());
				answerOptionDao.create(answerOption);
			}
		}
		return survey;
	}
	

}
