package service;

import java.util.List;

import dao.UserDao;
import dao.impl.UserDaoImpl;
import entity.User;
import entity.User.Role;

public class UserServiceImpl implements UserService {
	
	protected UserDao userDao;
	
	public UserServiceImpl() {
		userDao = new UserDaoImpl();
    }

	@Override
	public User authenticate(String email, String password) {
		User user = userDao.get(email);
		
		if(!password.equals(user.getPassword())) {
			user.setPassword("");
		}
		
		return user;
	}

	@Override
	public void changeRole(User user, Role role) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changePassword(User user, String newPassword) {
		user.setPassword(newPassword);
		userDao.update(user);
		
	}

	@Override
	public User create(User user) {
		userDao.create(user);
		return user;
	}

	@Override
	public List<User> getAll() {
		List<User> users = userDao.getAll();
		return users;
	}	

}
