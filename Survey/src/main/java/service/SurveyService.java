package service;

import java.util.List;

import entity.Survey;
import entity.User;
import entity.UserSurvey;

public interface SurveyService {
	
	Survey create(Survey survey);
	
	Survey get(Long surveyId);
	
	Survey update(Survey survey);
	
	UserSurvey save(Survey survey, User user, List<Long> answers);
	
	List<Survey> available(Long userId);
	
	void delete(Survey survey);

	List<Survey> passed(Long userId);
	
	List<Survey> getAll();

}
