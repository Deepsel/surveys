<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Личный кабинет - Профиль</title>
	<base href="${pageContext.request.contextPath}/">
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="resources/css/cabinet.css" rel="stylesheet">
    <script src="resources/js/bootstrap.min.js"></script>
    
</head>


<body>
    <nav class="navbar navbar-light bg-light justify-content-between">
        <a class="navbar-brand"><img src="resources/images/logo.png" alt=""></a>
        <div class="nav navbar-nav navbar-right">
            <a class="btn btn-outline-primary my-2 my-sm-0" href="tab/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Выйти</a>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block sidebar">
            <c:choose>
		<c:when test="${authenticated.role == 'ADMIN'}">
			<ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" href="tab/admin?id=${authenticated.id}">Профиль</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/all">Сотрудники</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/addemployee">Добавить сотрудника</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/alldepartment">Отделы</a>
                    </li>
                </ul>
		</c:when>
		<c:otherwise>
			<ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" href="tab/employee?id=${authenticated.id}">Профиль</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/all">Сотрудники</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/alldepartment">Отделы</a>
                    </li>
                    
                </ul>
		</c:otherwise>
	</c:choose>
                
            </nav>

            <div class="col-sm-9 ml-sm-auto col-md-10 pt-3">
                <h1>${authenticated.surname} ${authenticated.name} ${authenticated.patronymic}</h1>
                <hr>
                <div class="jumbotron">
                    <p class="paramentr">Дата рождения: <span class="trait">${authenticated.birthday}</span></p>
                    <p class="paramentr">Пол: <span class="trait"><c:if test = "${authenticated.sex == 'FEMALE'}">Женский</c:if>
                    <c:if test = "${authenticated.sex == 'MALE'}">Мужской</c:if></span></p>
                    <p class="paramentr">Телефонный номер: <span class="trait">${authenticated.number}</span></p>
                    <p class="paramentr">Email: <span class="trait">${authenticated.email}</span></p>
                    <p class="paramentr">Серия и номер паспорта: <span class="trait">${authenticated.passport}</span></p>
                    <p class="paramentr">Домашний адрес: <span class="trait">${authenticated.address}</span></p>
                    <p class="paramentr">Место работы: <span class="trait">Отдел ${authenticated.department.name}</span></p>
                    <p class="paramentr">Должность: <span class="trait">${authenticated.position}</span></p>
                    <p class="paramentr">Стаж: 
                    <fmt:formatNumber var="exp" minIntegerDigits="1" maxFractionDigits="1" value="${authenticated.experiece/12}" />
                    <span class="trait"> ${exp} лет</span></p>
                    <a class="btn btn-primary" href="tab/edit" role="button">Редактировать</a>
                    <a class="btn btn-primary" href="tab/editpass" role="button">Сменить пароль</a>
                </div>
            </div>
        </div>
    </div>
    
  <footer class="footer">
      <div class="container">
        
      </div>
    </footer>

</body>

</html>