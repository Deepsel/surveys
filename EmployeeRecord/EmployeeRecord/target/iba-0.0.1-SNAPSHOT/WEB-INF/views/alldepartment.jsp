<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Отделы</title>
	<base href="${pageContext.request.contextPath}/">
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="resources/css/cabinet.css" rel="stylesheet">
    <script src="resources/js/bootstrap.min.js"></script>
</head>


<body>
    <nav class="navbar navbar-light bg-light justify-content-between">
        <a class="navbar-brand"><img src="resources/images/logo.png" alt=""></a>
        <div class="nav navbar-nav navbar-right">
            <a class="btn btn-outline-primary my-2 my-sm-0" href="tab/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Выйти</a>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block sidebar">
            <c:choose>
		<c:when test="${authenticated.role == 'ADMIN'}">
			<ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="tab/admin?id=${authenticated.id}">Профиль</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/all">Сотрудники</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/addemployee">Добавить сотрудника</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="tab/alldepartment">Отделы</a>
                    </li>
                </ul>
		</c:when>
		<c:otherwise>
			<ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="tab/employee?id=${authenticated.id}">Профиль</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/all">Сотрудники</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="tab/alldepartment">Отделы</a>
                    </li>
                    
                </ul>
		</c:otherwise>
	</c:choose>
                
            </nav>

            <div class="col-md-6 pt-3">
                <h2> Отделы </h2>
                
    <table class="table table-striped table-bordered">
        <tbody>
            <tr>
                <td><b>Id</b></td>
                <td><b>Название</b></td>
                
            </tr>
            <c:forEach items="${departments}" var="department">
                <tr>
                    <td>${department.id}</td>
                    <td>Отдел ${department.name}</td> 
                </tr>
            </c:forEach>
        </tbody>
        </table>
            </div>         
      </div>
    </div>
    
  <footer class="footer">
      <div class="container">
        
      </div>
    </footer>

</body>

</html>