<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Список всех сотрудников</title>
	<base href="${pageContext.request.contextPath}/">
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="resources/css/cabinet.css" rel="stylesheet">
    <script src="resources/js/bootstrap.min.js"></script>
</head>


<body>
    <nav class="navbar navbar-light bg-light justify-content-between">
        <a class="navbar-brand"><img src="resources/images/logo.png" alt=""></a>
        <div class="nav navbar-nav navbar-right">
            <a class="btn btn-outline-primary my-2 my-sm-0" href="tab/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Выйти</a>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block sidebar">
            <c:choose>
		<c:when test="${authenticated.role == 'ADMIN'}">
			<ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="tab/admin?id=${authenticated.id}">Профиль</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="tab/all">Сотрудники</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/addemployee">Добавить сотрудника</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/alldepartment">Отделы</a>
                    </li>
                </ul>
		</c:when>
		<c:otherwise>
			<ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="tab/employee?id=${authenticated.id}">Профиль</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  active" href="tab/all">Сотрудники</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/alldepartment">Отделы</a>
                    </li>
                    
                </ul>
		</c:otherwise>
	</c:choose>
                
            </nav>

            <div class="col-sm-9 ml-sm-auto col-md-10 pt-3">
                <h2> Пользователи </h2>
                <form method="get" class="form-inline pull-right">
                <div class="input-group">
  <label class="mr-sm-2" for="inlineFormCustomSelect">Сортировать по</label>
  <select name="sortby" class="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect">
    <option selected value="id">id</option>
    <option value="surname">Фамилии</option>
    <option value="position">Должности</option>
    <option value="experience">Стажу</option>
  </select>

  <button type="submit" class="btn btn-primary">Сортировать</button>
  </div>
</form>
<br>
<br>
    <table class="table table-striped table-bordered">
        <tbody>
            <tr>
                <td><b>Id</b></td>
                <td><b>Фамилия</b></td>
                <td><b>Имя</b></td>
                <td><b>Отчество</b></td>
                <td><b>Должность</b></td>
                <td><b>Отдел</b></td>
                <td><b>Стаж</b></td>
            </tr>
            <c:forEach items="${employees}" var="employee">
            <c:if test="${authenticated.role == 'ADMIN'}">
                <tr class="tr-hover" onclick="window.document.location='tab/editemployee?id=${employee.id}';">
                
                    <td>${employee.id}</td>
                    <td>${employee.surname}</td>
                    <td>${employee.name}</td>
                    <td>${employee.patronymic}</td>
                    <td>${employee.position}</td>
                    <td>${employee.department.name}</td>
                    <fmt:formatNumber var="exp" minIntegerDigits="1" maxFractionDigits="1" value="${employee.experiece/12}" />
                    <td>${exp}</td>
                </tr>
                </c:if>
                
                <c:if test="${authenticated.role == 'EMPLOYEE'}">
                <tr>
                    <td>${employee.id}</td>
                    <td>${employee.surname}</td>
                    <td>${employee.name}</td>
                    <td>${employee.patronymic}</td>
                    <td>${employee.position}</td>
                    <td>${employee.department.name}</td>
                    <fmt:formatNumber var="exp" minIntegerDigits="1" maxFractionDigits="1" value="${employee.experiece/12}" />
                    <td>${exp}</td>
                </tr>
                </c:if>
            </c:forEach>
        </tbody>
        </table>
            </div>
        </div>
    </div>
    
  <footer class="footer">
      <div class="container">
        
      </div>
    </footer>

</body>

</html>