function emailvalid() {
    'use strict';
    var valid = false,
        r = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
    if (r.test(document.getElementById('inputEmail').value) && document.getElementById('inputEmail').value.length !== 0) {
        document.getElementById('inputEmail').className = 'form-control';
        valid = true;
    } else {
        document.getElementById('inputEmail').className = 'form-control invalid';
        valid = false;
    }
    return valid;
}

function passwordvalid() {
    'use strict';
    var valid;
    if (/\S/.test(document.getElementById('inputPassword').value) && document.getElementById('inputPassword').value.length !== 0) {
        document.getElementById('inputPassword').className = 'form-control';
        valid = true;
    } else {
        document.getElementById('inputPassword').className = 'form-control invalid';
        valid = false;
    }
    return valid;
}

function loginvalid() {
    'use strict';
    var arr = [];
    var valid = true;
    arr.push(emailvalid());
    arr.push(passwordvalid());
    var i;
    for (i = 0; i < arr.length; i++) {
        if (arr[i] === false) {
            valid = false;
            break;
        }
    }
    return valid;
}