package by.iba.web.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.reflections.Reflections;

import by.iba.web.annotation.Mapper;
import by.iba.web.util.RequestMaper;
public class ActionFactory {
	
	final static Logger logger = Logger.getLogger(ActionFactory.class);

	private static Map<String, Action> actions;

	static {
		actions = new HashMap<>();
		Reflections reflections = new Reflections("by.iba.web.action");
		Set<Class<? extends Action>> actionClass = reflections.getSubTypesOf(Action.class);
		actionClass.stream().forEach(action -> {
			try {
				actions.put(action.getAnnotation(Mapper.class).url(), action.newInstance());
			} catch (InstantiationException | IllegalAccessException e) {
				logger.error("Exception!" + e);
				throw new ActionException(e);
			}
		});
		
	}

	public static Action getAction(HttpServletRequest request) {
		String baseUrl = RequestMaper.getUrl(request);
		return actions.get(baseUrl);
	}

}
