package by.iba.web.util.validation;

public class LoginFormValidator {
	
	public boolean check(String email, String password) {
		boolean checked = false;
		EmailValidator emailValidator = new EmailValidator();
		PasswordValidator passwordValidator = new PasswordValidator();
		
		if (emailValidator.check(email) && passwordValidator.check(password)) {
			checked = true;
		}
		
		
		return checked;
	}

}
