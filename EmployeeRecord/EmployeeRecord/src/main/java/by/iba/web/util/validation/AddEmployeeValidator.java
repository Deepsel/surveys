package by.iba.web.util.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

public class AddEmployeeValidator {


	public boolean check(HttpServletRequest request) {
		boolean valid = true;
		boolean[] arr = new boolean[12];
		arr[0] = strValidate(request.getParameter("surname"));
		arr[1] = strValidate(request.getParameter("name"));
		arr[2] = strValidate(request.getParameter("patronymic"));
		arr[3] = dateValidate(request.getParameter("birthday"));
		arr[4] = sexValidate(request.getParameter("sex"));
		arr[5] = numberValidate(request.getParameter("number"));
		EmailValidator validator = new EmailValidator();
		arr[6] = validator.check(request.getParameter("email"));
		arr[7] = passportValidate(request.getParameter("passport"));
		arr[8] = strValidate(request.getParameter("address"));;
		arr[9] = decimalValidate(request.getParameter("department"));
		arr[10] = strValidate(request.getParameter("position"));
		arr[11] = decimalValidate(request.getParameter("experience"));;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == false) {
				valid = false;
				break;
			}

		}
		return valid;

	}
	
	public boolean strValidate(String str) {
		boolean valid;
		Pattern pattern = Pattern.compile("[\\s]{2,}");
		Matcher matcher = pattern.matcher(str);
		if (matcher.find() || str.isEmpty() || str.equals(null)) {
			valid = false;
		} else {
			valid = true;
		}
		return valid;
	}
	
	public boolean dateValidate(String str) {
		boolean valid;
		Pattern pattern = Pattern.compile("(19|20)[0-9]{2}-[0|1][0-9]-[0-3][0-9]");
		Matcher matcher = pattern.matcher(str);
		if (matcher.find()) {
			valid = true;
		} else {
			valid = false;
		}
		return valid;
	}
	
	public boolean sexValidate(String sex) {
		boolean valid;
		if ("MALE".equals(sex) || "FEMALE".equals(sex)) {
			valid = true;
		} else {
			valid = false;
		}
		return valid;
	}
	
	public boolean numberValidate(String number) {
		boolean valid;
		Pattern pattern = Pattern.compile("[\\d]{12}");
		Matcher matcher = pattern.matcher(number);
		if (matcher.find()) {
			valid = true;
		} else {
			valid = false;
		}
		return valid;
	}
	
	public boolean passportValidate(String passport) {
		boolean valid;
		Pattern pattern = Pattern.compile("([A-Z]{2})([\\d]{7})");
		Matcher matcher = pattern.matcher(passport);
		if (matcher.find()) {
			valid = true;
		} else {
			valid = false;
		}
		return valid;
	}
	
	public boolean decimalValidate(String str) {
		boolean valid;
		Pattern pattern = Pattern.compile("([\\d]{1,}.{0,1}[\\d]{0,1})");
		Matcher matcher = pattern.matcher(str);
		if (matcher.find()) {
			valid = true;
		} else {
			valid = false;
		}
		return valid;
	}
	
}