package by.iba.web.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.iba.entity.Department;
import by.iba.entity.Employee.Role;
import by.iba.web.annotation.Mapper;
import by.iba.web.annotation.Security;

@Security({Role.ADMIN, Role.EMPLOYEE})
@Mapper(url="/tab/alldepartment")
public class AllDepartment extends Action {

	@Override
	public String execute(HttpServletRequest request) {
		String view = "alldepartment";
		List<Department> departments = departmentService.getAll();
		request.setAttribute("departments", departments);
		return view;
	}

}
