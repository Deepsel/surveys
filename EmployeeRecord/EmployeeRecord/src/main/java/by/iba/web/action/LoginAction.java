package by.iba.web.action;

import javax.servlet.http.HttpServletRequest;

import by.iba.entity.Employee;
import by.iba.entity.Employee.Role;
import by.iba.web.annotation.Mapper;
import by.iba.web.annotation.Security;
import by.iba.web.util.validation.LoginFormValidator;


@Security({Role.ANONYM})
@Mapper(url = "/tab/login")
public class LoginAction extends Action {

	@Override
	public String execute(HttpServletRequest request) {
		String method = request.getMethod();
		String view = method.equals("POST") ? post(request) : get(request);
		return view;
	}

	private String get(HttpServletRequest request) {

		return "../../index";
	}

	private String post(HttpServletRequest request) {
		String view = "../../index";
		LoginFormValidator validator = new LoginFormValidator();
		String email = request.getParameter("email");
		String password = request.getParameter("password");

		if (validator.check(email, password)) {
			Employee employee = employeeService.authenticate(email, password);
			if (!employee.getPassword().isEmpty()) {
				request.getSession().setAttribute("authenticated", employee);
				view = Role.ADMIN.equals(employee.getRole()) ? "redirect:/tab/admin?id=" + employee.getId()
						: "redirect:/tab/employee?id=" + employee.getId();
			} else {
				request.setAttribute("error", "true");
				request.setAttribute("email", email);
			}
		}

		return view;
	}

}
