package by.iba.web.action;

import javax.servlet.http.HttpServletRequest;

import by.iba.entity.Employee.Role;
import by.iba.web.annotation.Mapper;
import by.iba.web.annotation.Security;


@Security({Role.EMPLOYEE})
@Mapper(url = "/tab/employee")
public class EmployeeCabinet extends Action {

	@Override
	public String execute(HttpServletRequest request) {
		
		return "cabinet";
	}

}
