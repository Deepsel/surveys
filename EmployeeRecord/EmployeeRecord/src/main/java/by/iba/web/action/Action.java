package by.iba.web.action;

import javax.servlet.http.HttpServletRequest;

import by.iba.service.DepartmentService;
import by.iba.service.DepartmentServiceImpl;
import by.iba.service.EmployeeService;
import by.iba.service.EmployeeServiceImpl;

public abstract class Action {
	
    protected EmployeeService employeeService;
    protected DepartmentService departmentService;
    
    public Action() {
    	employeeService = new EmployeeServiceImpl();
    	departmentService = new DepartmentServiceImpl();
    }
	
	abstract public String execute(HttpServletRequest request);

}
