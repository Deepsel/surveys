package by.iba.web.action;

import javax.servlet.http.HttpServletRequest;

import by.iba.entity.Employee;
import by.iba.entity.Employee.Role;
import by.iba.web.annotation.Mapper;
import by.iba.web.annotation.Security;
import by.iba.web.util.validation.PasswordValidator;


@Security({Role.ADMIN, Role.EMPLOYEE})
@Mapper(url = "/tab/editpass")
public class ChangePasswordAction extends Action {

	@Override
	public String execute(HttpServletRequest request) {
		String method = request.getMethod();
		String view = method.equals("POST") ? post(request) : get(request);
		return view;
	}

	private String get(HttpServletRequest request) {

		return "editpass";
	}

	private String post(HttpServletRequest request) {
		String view = "";
		Employee employee = (Employee) request.getSession().getAttribute("authenticated");
		PasswordValidator validator = new PasswordValidator();
		String oldpass = request.getParameter("oldpassword");
		String newpass = request.getParameter("newpassword");
		boolean valid = validator.check(oldpass) && validator.check(newpass);
		if (valid && oldpass.equals(employee.getPassword())) {
			employeeService.changePassword(employee, newpass);
			view = "redirect:/tab/editpass";
		} else {

			view = "editpass";
			request.setAttribute("error", "true");

		}
		return view;

	}
}
