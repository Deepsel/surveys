package by.iba.web.action;

import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.iba.dao.exception.DaoException;
import by.iba.entity.Department;
import by.iba.entity.Employee;
import by.iba.entity.Employee.Role;
import by.iba.entity.Employee.Sex;
import by.iba.web.annotation.Mapper;
import by.iba.web.annotation.Security;
import by.iba.web.util.validation.AddEmployeeValidator;

@Security({Role.ADMIN})
@Mapper(url="/tab/editemployee")
public class EditEmployee extends Action {

	@Override
	public String execute(HttpServletRequest request) {
		String method = request.getMethod();
		String view = method.equals("POST") ? post(request) : get(request);
		return view;
	}

	private String get(HttpServletRequest request) {
		String view = "editemployee";
		List<Department> departments = departmentService.getAll();
		Employee employee = new Employee();
		if(request.getParameter("id") != null) {
			Long id = Long.parseLong(request.getParameter("id"));
			employee = employeeService.get(id);
			request.setAttribute("employee", employee);
		} else {
			view = "redirect:/tab/allemployee";
		}
		request.setAttribute("departments", departments);
		return view;
	}

	private String post(HttpServletRequest request) {
		String view = "addemployee";
		Employee employee = new Employee();
		Long id = Long.parseLong(request.getParameter("id"));
		String oldpass = employeeService.get(id).getPassword();
		List<Department> departments = departmentService.getAll();

		AddEmployeeValidator validator = new AddEmployeeValidator();
		employee.setId(Long.valueOf(request.getParameter("id")));
		employee.setSurname(request.getParameter("surname"));
		employee.setName(request.getParameter("name"));
		employee.setPatronymic(request.getParameter("patronymic"));
		employee.setNumber(request.getParameter("number"));
		employee.setEmail(request.getParameter("email"));
		employee.setPassport(request.getParameter("passport"));
		employee.setAddress(request.getParameter("address"));
		employee.setPosition(request.getParameter("position"));
		employee.setRole(Role.valueOf(request.getParameter("role")));

		if (validator.check(request)) {
			employee.setSex(Sex.valueOf(request.getParameter("sex")));
			employee.setBirthday(Date.valueOf(request.getParameter("birthday")));
			employee.setPassword(oldpass);
			employee.setDepartment(departmentService.get(Long.parseLong(request.getParameter("department"))));
			double exp = Double.parseDouble(request.getParameter("experience").replace(",", "."));
			Long experience = Math.round(exp * 12);
			employee.setExperiece(experience);
			try {
				employee = employeeService.update(employee);
				view = "redirect:/tab/all";
			} catch (DaoException e) {
				request.setAttribute("employee", employee);
				request.setAttribute("dberror", "true");
				request.setAttribute("departments", departments);
			}

		} else {
			request.setAttribute("employee", employee);
			request.setAttribute("error", "true");
			request.setAttribute("departments", departments);
		}

		return view;

	}

}
