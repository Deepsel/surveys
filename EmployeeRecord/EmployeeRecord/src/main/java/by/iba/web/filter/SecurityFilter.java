package by.iba.web.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.reflections.Reflections;

import by.iba.entity.Employee;
import by.iba.entity.Employee.Role;
import by.iba.web.action.Action;
import by.iba.web.annotation.Mapper;
import by.iba.web.annotation.Security;

@WebFilter(urlPatterns = "/tab/*")
public class SecurityFilter implements Filter{
	
	private Map<String, List<Role>> availability;

	

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;
        
        Employee employee = (Employee)req.getSession().getAttribute("authenticated");
        
        if (employee == null) {
        	employee = new Employee();
        	employee.setRole(Role.ANONYM);
            req.getSession().setAttribute("authenticated", employee);
        }
        List<Role> availableRoles = availability.get("/tab"+ req.getPathInfo());
        
        if (!availableRoles.contains(employee.getRole())) {
            res.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        
        chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		availability = new HashMap<>();
		Reflections reflections = new Reflections("by.iba.web.action");
		Set<Class<? extends Action>> actionClass = reflections.getSubTypesOf(Action.class);
		actionClass.stream().forEach(action -> {
			Role[] roles;
			if (action.isAnnotationPresent(Security.class)) {
			    Security security = action.getAnnotation(Security.class);
			    roles = security.value();
			} else {
				roles = Role.values();
			}
			availability.put(action.getAnnotation(Mapper.class).url(), Arrays.asList(roles));
		});
		
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	

}
