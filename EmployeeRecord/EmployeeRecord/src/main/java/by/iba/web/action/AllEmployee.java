package by.iba.web.action;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.iba.entity.Employee;
import by.iba.entity.Employee.Role;
import by.iba.web.annotation.Mapper;
import by.iba.web.annotation.Security;

@Security({Role.ADMIN, Role.EMPLOYEE})
@Mapper(url="/tab/all")
public class AllEmployee extends Action {

	@Override
	public String execute(HttpServletRequest request) {
		String view = "all";
		List<Employee> employees = employeeService.getAll();
	if(request.getParameter("sortby") != null) {
		switch (request.getParameter("sortby")) {
		case "surname": 
			Collections.sort(employees, Employee.COMPARE_BY_SURNAME);
			break;
		case "position": 
			Collections.sort(employees, Employee.COMPARE_BY_POSITION);
			break;
		case "experience": 
			Collections.sort(employees, Employee.COMPARE_BY_EXPERIENCE);
			break;
		default:
			break;
		}
	}
		request.setAttribute("employees", employees);
		return view;

	}
	
}
