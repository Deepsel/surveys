package by.iba.web.util;

import javax.servlet.http.HttpServletRequest;

public class RequestMaper {

	public static String getUrl(HttpServletRequest request) {

		String uri = request.getRequestURI();
		String path = uri.replaceAll(".jsp", "").trim();
		String a = request.getContextPath();
		String resultPath = path.substring(a.length());
		
		return resultPath;
	}

}
