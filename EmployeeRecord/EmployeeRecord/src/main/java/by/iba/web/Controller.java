package by.iba.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.iba.web.action.Action;
import by.iba.web.action.ActionFactory;

@WebServlet("/tab/*")
public class Controller extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		Action action = ActionFactory.getAction(req);
		String view = action.execute(req);

		if (!view.startsWith("redirect:")) {
			req.getRequestDispatcher("/WEB-INF/views/" + view + ".jsp").forward(req, resp);
		} else {
			resp.sendRedirect(req.getContextPath() + view.substring(9));
		}

	}
}
