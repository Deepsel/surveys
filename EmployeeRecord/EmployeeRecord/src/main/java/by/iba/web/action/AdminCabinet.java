package by.iba.web.action;

import javax.servlet.http.HttpServletRequest;

import by.iba.entity.Employee.Role;
import by.iba.web.annotation.Mapper;
import by.iba.web.annotation.Security;

@Security({Role.ADMIN})
@Mapper(url = "/tab/admin")
public class AdminCabinet extends Action {

	@Override
	public String execute(HttpServletRequest request) {
		
		return "cabinet";
	}

}
