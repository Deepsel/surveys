package by.iba.web.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.iba.entity.Employee.Role;
import by.iba.web.annotation.Mapper;
import by.iba.web.annotation.Security;

@Security({Role.ADMIN, Role.EMPLOYEE})
@Mapper(url = "/tab/logout")
public class LogoutAction extends Action {

	@Override
	public String execute(HttpServletRequest request) {
		String view =  "redirect:";
		HttpSession session = request.getSession();

		if (!session.isNew()) {
		    session.invalidate();
		    session = request.getSession();
		}
		return view;
	}

}
