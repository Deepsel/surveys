package by.iba.entity;

import java.io.Serializable;
import java.sql.Date;

public class WorkExperience implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long employeeId;
	private Date joinDate;
	private Long joinExp;
	
	
	public WorkExperience() {
		
	}
	
	public WorkExperience(Long employeeId, Date joinDate, Long joinExp) {
		this.employeeId = employeeId;
		this.joinDate = joinDate;
		this.joinExp = joinExp;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((employeeId == null) ? 0 : employeeId.hashCode());
		result = prime * result + ((joinDate == null) ? 0 : joinDate.hashCode());
		result = prime * result + ((joinExp == null) ? 0 : joinExp.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkExperience other = (WorkExperience) obj;
		if (employeeId == null) {
			if (other.employeeId != null)
				return false;
		} else if (!employeeId.equals(other.employeeId))
			return false;
		if (joinDate == null) {
			if (other.joinDate != null)
				return false;
		} else if (!joinDate.equals(other.joinDate))
			return false;
		if (joinExp == null) {
			if (other.joinExp != null)
				return false;
		} else if (!joinExp.equals(other.joinExp))
			return false;
		return true;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public Long getJoinExp() {
		return joinExp;
	}
	public void setJoinExp(Long joinExp) {
		this.joinExp = joinExp;
	}

}
