package by.iba.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import by.iba.dao.exception.DaoException;
import by.iba.web.action.ActionFactory;

public class ConnectionPool {
	final static Logger logger = Logger.getLogger(ActionFactory.class);

	private static final ConnectionPool pool = new ConnectionPool();

	private DataSource dataSource;

	public static ConnectionPool getPool() {
		return pool;
	}

	private ConnectionPool() {

		try {
			Context initContext = new InitialContext();
			Context rootContext = (Context) initContext.lookup("java:comp/env");
			dataSource = (DataSource) rootContext.lookup("jdbc/employeerecord");
		} catch (NamingException e) {
			logger.error("Some errors occurred during DataSource lookup!" + e);
			throw new DaoException(e);
		}

	}

	public Connection getConnection() {
		try {
			return dataSource.getConnection();
		} catch (SQLException e) {
			logger.error("Can not receive connection!" + e);
			throw new DaoException(e);
		}
	}

	public void closeDbResources(Connection connection, Statement statement) {
		closeDbResources(connection, statement, null);
	}

	public void closeDbResources(Connection connection, Statement statement, ResultSet resultSet) {
		closeResultSet(resultSet);
		closeStatement(statement);
		closeConnection(connection);
	}

	private void closeConnection(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error("Error: Connection has not been closed!" + e);
			}
		}
	}

	private void closeStatement(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				logger.error("Error: Statement has not been closed!" + e);
			}
		}
	}

	private void closeResultSet(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				logger.error("Error: ResultSet has not been closed!" + e);
			}
		}
	}

}
