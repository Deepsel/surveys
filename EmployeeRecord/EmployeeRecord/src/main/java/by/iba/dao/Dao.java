package by.iba.dao;

import java.util.List;

import by.iba.dao.exception.DaoException;

public interface Dao<T> {
	
	T create(T t) throws DaoException;

	T update(T t) throws DaoException;

	void delete(Long id) throws DaoException;

	T get(Long id) throws DaoException;
	
	List<T> getAll() throws DaoException;

}
