package by.iba.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.iba.dao.Dao;
import by.iba.dao.exception.DaoException;
import by.iba.dao.util.ConnectionPool;
import by.iba.entity.WorkExperience;

public class WorkExperienceDao implements Dao<WorkExperience> {

	private ConnectionPool pool;

	public WorkExperienceDao() {
		pool = ConnectionPool.getPool();
	}

	@Override
	public WorkExperience create(WorkExperience t) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(
					"INSERT INTO `employeerecord`.`workexperience` (`employeeId`, `joinDate`, `joinExp`) VALUES (?, ?, ?);");
			statement.setLong(1, t.getEmployeeId());
			statement.setDate(2, t.getJoinDate());
			statement.setLong(3, t.getJoinExp());
			statement.execute();

		} catch (SQLException e) {
			new DaoException(e);
		}

		return t;
	}

	@Override
	public WorkExperience update(WorkExperience t) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(
					"UPDATE `employeerecord`.`workexperience` SET `joinDate` = ?, `joinExp` = ? WHERE `employeeId` = ?;");
			statement.setDate(1, t.getJoinDate());
			statement.setLong(2, t.getJoinExp());
			statement.setLong(3, t.getEmployeeId());
			statement.execute();
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement);
		}
		return t;
	}

	@Override
	public void delete(Long id) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = connection
					.prepareStatement("DELETE FROM `employeerecord`.`workexperience` WHERE`employeeId` = ?;");
			statement.setLong(1, id);
			statement.execute();
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement);
		}

	}

	@Override
	public WorkExperience get(Long id) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		WorkExperience t = new WorkExperience();

		try {
			statement = connection
					.prepareStatement("SELECT * FROM `employeerecord`.`workexperience` WHERE `employeeId` = ?;");
			statement.setLong(1, id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				t.setEmployeeId(id);
				t.setJoinDate(resultSet.getDate("joinDate"));
				t.setJoinExp(resultSet.getLong("joinExp"));
			}

		} catch (SQLException e) {
			new DaoException(e);
		}

		return t;
	}

	@Override
	public List<WorkExperience> getAll() throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<WorkExperience> experiences = new ArrayList<WorkExperience>();

		try {
			statement = connection.prepareStatement("SELECT * FROM `employeerecord`.`workexperience`;");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				WorkExperience t = new WorkExperience();
				t.setEmployeeId(resultSet.getLong("employeeId"));
				t.setJoinDate(resultSet.getDate("joinDate"));
				t.setJoinExp(resultSet.getLong("joinExp"));
			}
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement, resultSet);
		}

		return experiences;
	}

	public Long getEmloyeeExperience(Long employeeId) {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Long experience = null;
		try {
			statement = connection.prepareStatement(
					"SELECT ((DATEDIFF(DATE(NOW()),DATE(joinDate))/30.42) + joinExp) as Experience FROM `employeerecord`.`workexperience` WHERE employeeId = ?;");
			statement.setLong(1, employeeId);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				experience = resultSet.getLong("experience");
			}
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement, resultSet);
		}

		return experience;

	}

}
