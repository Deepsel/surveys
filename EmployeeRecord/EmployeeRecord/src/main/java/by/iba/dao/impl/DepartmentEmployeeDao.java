package by.iba.dao.impl;

import by.iba.entity.DepartmentEmployee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.iba.dao.Dao;
import by.iba.dao.exception.DaoException;
import by.iba.dao.util.ConnectionPool;

public class DepartmentEmployeeDao implements Dao<DepartmentEmployee> {

	private ConnectionPool pool;

	public DepartmentEmployeeDao() {
		pool = ConnectionPool.getPool();
	}
	@Override
	public DepartmentEmployee create(DepartmentEmployee t) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement("INSERT INTO `employeerecord`.`depemp` (`departmentId`, `employeeId`) VALUES (?, ?);");
			statement.setLong(1, t.getDepartmentId());
			statement.setLong(2, t.getEmployeeId());
			statement.execute();
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement);
		}
		return t;
	}
	
	public Long getEmployeeDepartment(Long employeeId) {
		
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Long departmentId = null;
		
		try {
			statement = connection.prepareStatement("SELECT `departmentId` FROM `employeerecord`.`depemp` WHERE `employeeId` = ?;");
			statement.setLong(1, employeeId);
			resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
				departmentId = resultSet.getLong("departmentId");
			}
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement, resultSet);
		}
		
		return departmentId;
		
	}
	
	public List<DepartmentEmployee> getByDepartmentId(Long departmentId){
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<DepartmentEmployee> departmentEmployees = new ArrayList<DepartmentEmployee>();
		
		try {
			statement = connection.prepareStatement("SELECT * FROM `employeerecord`.`depemp` WHERE `departmentId` = ?;");
			statement.setLong(1, departmentId);
			resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
				departmentEmployees.add(new DepartmentEmployee(departmentId, resultSet.getLong("employeeId")));
			}
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement, resultSet);
		}
		
		
		return departmentEmployees;
		
	}
	@Override
	public DepartmentEmployee update(DepartmentEmployee t) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement("UPDATE `employeerecord`.`depemp` SET `departmentId` = ? WHERE `employeeId` = ?;");
			statement.setLong(1, t.getDepartmentId());
			statement.setLong(2, t.getEmployeeId());
			statement.execute();
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement);
		}
		return t;
	}
	@Override
	public void delete(Long id) throws DaoException {
		
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement("DELETE FROM `employeerecord`.`depemp` WHERE`employeeId` = ?;");
			statement.setLong(1, id);
			statement.execute();
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement);
		}
	}

	@Override
	public DepartmentEmployee get(Long id) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DepartmentEmployee> getAll() throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}

}
