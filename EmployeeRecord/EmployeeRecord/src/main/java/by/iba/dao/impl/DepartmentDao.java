package by.iba.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.iba.dao.Dao;
import by.iba.dao.exception.DaoException;
import by.iba.dao.util.ConnectionPool;
import by.iba.entity.Department;

public class DepartmentDao implements Dao<Department> {
	
	private ConnectionPool pool;
	
	public DepartmentDao() {
		pool = ConnectionPool.getPool();
	}

	@Override
	public Department create(Department t) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement("INSERT INTO `employeerecord`.`department` (`depname`) VALUES (?);");
			statement.setString(1, t.getName());
			statement.execute();
			
		} catch (SQLException e) {
			new DaoException(e);
		}
		
		return t;
	}

	@Override
	public Department update(Department t) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement("UPDATE `employeerecord`.`department` SET `depname` = ? WHERE `id`=?;");
			statement.setString(1, t.getName());
			statement.setLong(2, t.getId());
			statement.execute();
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement);
		}
		
		return t;
	}

	@Override
	public void delete(Long id) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement("DELETE FROM `employeerecord`.`department` WHERE `id`= ?;");
			statement.setLong(1, id);
			statement.execute();
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement);
		}
		
		
	}

	@Override
	public Department get(Long id) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Department t = new Department();
		try {
			statement = connection.prepareStatement("SELECT * FROM `employeerecord`.`department` where id = ?;");
			statement.setLong(1, id);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				t.setId(id);
				t.setName(resultSet.getString("depname"));
			}
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement, resultSet);
		}
		
		return t;
	}

	@Override
	public List<Department> getAll() throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Department> departments = new ArrayList<Department>();
		
		try {
			statement = connection.prepareStatement("SELECT * FROM `employeerecord`.`department`;");
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				Department t = new Department();
				t.setId(resultSet.getLong("id"));
				t.setName(resultSet.getString("depname"));
				departments.add(t);
			}
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement, resultSet);
		}
		
		
		return departments;
	}

}
