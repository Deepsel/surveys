package by.iba.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.iba.dao.Dao;
import by.iba.dao.exception.DaoException;
import by.iba.dao.util.ConnectionPool;
import by.iba.entity.Employee;
import by.iba.entity.Employee.Role;
import by.iba.entity.Employee.Sex;

public class EmployeeDao implements Dao<Employee> {

	private ConnectionPool pool;

	public EmployeeDao() {
		pool = ConnectionPool.getPool();
	}

	@Override
	public Employee create(Employee t) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet generatedKeys = null;
		try {
			statement = connection.prepareStatement("INSERT INTO `employeerecord`.`employee`"
					+ " (`name`, `surname`, `patronymic`, `birthday`, `sex`, `position`, `number`, `passport`, `address`, `email`, `password`, `role`)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS);
			statement.setString(1, t.getName());
			statement.setString(2, t.getSurname());
			statement.setString(3, t.getPatronymic());
			statement.setDate(4, t.getBirthday());
			statement.setString(5, t.getSex().name());
			statement.setString(6, t.getPosition());
			statement.setString(7, t.getNumber());
			statement.setString(8, t.getPassport());
			statement.setString(9, t.getAddress());
			statement.setString(10, t.getEmail());
			statement.setString(11, t.getPassword());
			statement.setString(12, t.getRole().name());
			statement.execute();
			generatedKeys = statement.getGeneratedKeys();
			while(generatedKeys.next()) {
				t.setId(generatedKeys.getLong(1));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement);
		}
		return t;
	}

	@Override
	public Employee update(Employee t) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement("UPDATE `employeerecord`.`employee` SET"
					+ " `name` = ?, `surname` = ?, `patronymic` = ?, `birthday` = ?, `sex` = ?, `position` = ?,"
					+ " `number` = ?, `passport` = ?, `address` = ?, `email` = ?, `password` = ?, `role` = ?"
					+ " WHERE `id`=" + t.getId() + ";");
			statement.setString(1, t.getName());
			statement.setString(2, t.getSurname());
			statement.setString(3, t.getPatronymic());
			statement.setDate(4, t.getBirthday());
			statement.setString(5, t.getSex().name());
			statement.setString(6, t.getPosition());
			statement.setString(7, t.getNumber());
			statement.setString(8, t.getPassport());
			statement.setString(9, t.getAddress());
			statement.setString(10, t.getEmail());
			statement.setString(11, t.getPassword());
			statement.setString(12, t.getRole().name());
			statement.execute();
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement);
		}
		return t;
	}

	@Override
	public void delete(Long id) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement("DELETE FROM `employeerecord`.`employee` where id = ?;");
			statement.setLong(1, id);
			statement.execute();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement);
		}

	}

	@Override
	public Employee get(Long id) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Employee t = new Employee();

		try {
			statement = connection.prepareStatement("SELECT * FROM `employeerecord`.`employee` where id =?;");
			statement.setLong(1, id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				t.setId(id);
				t.setName(resultSet.getString("name"));
				t.setSurname(resultSet.getString("surname"));
				t.setPatronymic(resultSet.getString("patronymic"));
				t.setBirthday(resultSet.getDate("birthday"));
				t.setSex(Sex.valueOf(resultSet.getString("sex")));
				t.setPosition(resultSet.getString("position"));
				t.setNumber(resultSet.getString("number"));
				t.setPassport(resultSet.getString("passport"));
				t.setAddress(resultSet.getString("address"));
				t.setEmail(resultSet.getString("email"));
				t.setPassword(resultSet.getString("password"));
				t.setRole(Role.valueOf(resultSet.getString("role")));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement, resultSet);
		}

		return t;
	}
	
	public Employee get(String  email) throws DaoException {
		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Employee t = new Employee();

		try {
			statement = connection.prepareStatement("SELECT * FROM `employeerecord`.`employee` where email = ?;");
			statement.setString(1, email);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				t.setId(resultSet.getLong("id"));
				t.setName(resultSet.getString("name"));
				t.setSurname(resultSet.getString("surname"));
				t.setPatronymic(resultSet.getString("patronymic"));
				t.setBirthday(resultSet.getDate("birthday"));
				t.setSex(Sex.valueOf(resultSet.getString("sex")));
				t.setPosition(resultSet.getString("position"));
				t.setNumber(resultSet.getString("number"));
				t.setPassport(resultSet.getString("passport"));
				t.setAddress(resultSet.getString("address"));
				t.setEmail(resultSet.getString("email"));
				t.setPassword(resultSet.getString("password"));
				t.setRole(Role.valueOf(resultSet.getString("role")));
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement, resultSet);
		}

		return t;
	}

	@Override
	public List<Employee> getAll() throws DaoException {

		Connection connection = pool.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Employee> employees = new ArrayList<Employee>();

		try {
			statement = connection.prepareStatement("SELECT * FROM `employeerecord`.`employee`;");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Employee t = new Employee();
				t.setId(resultSet.getLong("id"));
				t.setName(resultSet.getString("name"));
				t.setSurname(resultSet.getString("surname"));
				t.setPatronymic(resultSet.getString("patronymic"));
				t.setBirthday(resultSet.getDate("birthday"));
				t.setSex(Sex.valueOf(resultSet.getString("sex")));
				t.setPosition(resultSet.getString("position"));
				t.setNumber(resultSet.getString("number"));
				t.setPassport(resultSet.getString("passport"));
				t.setAddress(resultSet.getString("address"));
				t.setEmail(resultSet.getString("email"));
				t.setPassword(resultSet.getString("password"));
				t.setRole(Role.valueOf(resultSet.getString("role")));
				employees.add(t);
			}
		} catch (SQLException e) {
			new DaoException(e);
		} finally {
			pool.closeDbResources(connection, statement, resultSet);
		}

		return employees;
	}

}
