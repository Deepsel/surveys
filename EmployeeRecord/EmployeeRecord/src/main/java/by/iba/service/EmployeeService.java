package by.iba.service;

import java.util.List;

import by.iba.entity.Employee;
import by.iba.entity.Employee.Role;

public interface EmployeeService {
	
    Employee authenticate(String email, String password);

    void changeRole(Employee employee, Role role);

    void changePassword(Employee employee, String newPassword);

    Employee create(Employee employee);
    
    Employee update(Employee employee);
    
    Employee get(Long id);
    
    Employee get(String email);
	
	List<Employee> getAll();
	
	void delete(Long id);
	
}
