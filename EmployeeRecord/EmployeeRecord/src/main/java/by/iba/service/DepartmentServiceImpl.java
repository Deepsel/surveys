package by.iba.service;

import java.util.List;

import by.iba.dao.impl.DepartmentDao;
import by.iba.entity.Department;

public class DepartmentServiceImpl implements DepartmentService {
	
	protected DepartmentDao departmentDao;
	
	public DepartmentServiceImpl() {
		departmentDao = new DepartmentDao();
	}

	@Override
	public Department create(Department department) {
		departmentDao.create(department);
		return department;
	}

	@Override
	public Department update(Department department) {
		department = departmentDao.update(department);
		return department;
	}

	@Override
	public void delete(Long id) {
		departmentDao.delete(id);
	}

	@Override
	public List<Department> getAll() {
		List<Department> departments = departmentDao.getAll();
		return departments;
	}

	@Override
	public Department get(Long id) {
		Department department = new Department();
		department = departmentDao.get(id);
		return department;
	}

}
