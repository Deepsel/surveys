package by.iba.service;

import java.util.List;

import by.iba.entity.Department;

public interface DepartmentService {
	
	Department create(Department department);
	
	Department update(Department department);
	
	void delete(Long id);

	Department get(Long id);
	
	List<Department> getAll();
}
