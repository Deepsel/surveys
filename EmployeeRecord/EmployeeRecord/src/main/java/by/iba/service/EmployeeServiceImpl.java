package by.iba.service;

import java.util.Date;
import java.util.List;

import by.iba.dao.impl.DepartmentDao;
import by.iba.dao.impl.DepartmentEmployeeDao;
import by.iba.dao.impl.EmployeeDao;
import by.iba.dao.impl.WorkExperienceDao;
import by.iba.entity.DepartmentEmployee;
import by.iba.entity.Employee;
import by.iba.entity.Employee.Role;
import by.iba.entity.WorkExperience;

public class EmployeeServiceImpl implements EmployeeService {

	protected EmployeeDao employeeDao;
	protected DepartmentDao departmentDao;
	protected WorkExperienceDao workExperienceDao;
	protected DepartmentEmployeeDao departmentEmployeeDao;

	public EmployeeServiceImpl() {
		employeeDao = new EmployeeDao();
		departmentDao = new DepartmentDao();
		workExperienceDao = new WorkExperienceDao();
		departmentEmployeeDao = new DepartmentEmployeeDao();
	}

	@Override
	public Employee authenticate(String email, String password) {
		Employee employee = employeeDao.get(email);
		employee.setExperiece(workExperienceDao.getEmloyeeExperience(employee.getId()));
		employee.setDepartment(departmentDao.get(departmentEmployeeDao.getEmployeeDepartment(employee.getId())));

		if (!password.equals(employee.getPassword())) {
			employee.setPassword("");
		}

		return employee;
	}

	@Override
	public void changeRole(Employee employee, Role role) {
		employee.setRole(role);
		employeeDao.update(employee);

	}

	@Override
	public void changePassword(Employee employee, String newPassword) {
		employee.setPassword(newPassword);
		employeeDao.update(employee);

	}

	@Override
	public Employee create(Employee employee) {
		employee = employeeDao.create(employee);
		workExperienceDao
				.create(new WorkExperience(employee.getId(),new java.sql.Date(new Date().getTime()), employee.getExperiece()));
		departmentEmployeeDao.create(new DepartmentEmployee(employee.getDepartment().getId(), employee.getId()));
		return employee;
	}

	@Override
	public Employee get(Long id) {
		Employee employee = new Employee();
		employee = employeeDao.get(id);
		employee.setExperiece(workExperienceDao.getEmloyeeExperience(id));
		employee.setDepartment(departmentDao.get(departmentEmployeeDao.getEmployeeDepartment(id)));
		return employee;
	}

	@Override
	public List<Employee> getAll() {
		List<Employee> employees = employeeDao.getAll();
		
		for(Employee employee : employees) {
			employee.setExperiece(workExperienceDao.getEmloyeeExperience(employee.getId()));
			employee.setDepartment(departmentDao.get(departmentEmployeeDao.getEmployeeDepartment(employee.getId())));
		}
			
		return employees;
	}

	@Override
	public Employee get(String email) {
		Employee employee = employeeDao.get(email);
		return employee;
	}

	@Override
	public Employee update(Employee employee) {
		employeeDao.update(employee);
		workExperienceDao
		.update(new WorkExperience(employee.getId(),new java.sql.Date(new Date().getTime()), employee.getExperiece()));
		departmentEmployeeDao.update(new DepartmentEmployee(employee.getDepartment().getId(), employee.getId()));
		return employee;
	}

	@Override
	public void delete(Long id) {
		departmentEmployeeDao.delete(id);
		workExperienceDao.delete(id);
		employeeDao.delete(id);
		
	}

}
