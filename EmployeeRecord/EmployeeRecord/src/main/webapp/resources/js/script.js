function emailvalid() {
    'use strict';
    var valid = false,
        r = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
    if (r.test(document.getElementById('email').value) && document.getElementById('email').value.length !== 0) {
        document.getElementById('email').className = 'form-control';
        valid = true;
    } else {
        document.getElementById('email').className = 'form-control invalid';
        valid = false;
    }
    return valid;
}

function strValidate(str, RegExp) {
    'use strict';
    var valid = false;
    if (RegExp.test(document.getElementById(str).value) && document.getElementById(str).value.length !== 0) {
        document.getElementById(str).className = 'form-control';
        valid = true;
    } else {
        document.getElementById(str).className = 'form-control invalid';
        valid = false;
    }
    return valid;
}

function sexvalidate() {
    'use strict';
    var valid;
    if (document.getElementById('male').checked === true || document.getElementById('female').checked === true) {
        document.getElementById('sexm').className = 'custom-control-indicator';
        document.getElementById('sexf').className = 'custom-control-indicator';
        valid = true;
    } else {
        document.getElementById('sexm').className = 'custom-control-indicator invalid';
        document.getElementById('sexf').className = 'custom-control-indicator invalid';
        valid = false;
    }
    return valid;
}

function isValidDate(){
    'use strict';
    var valid,
        d = document.getElementById('birthday').value;
    if(!isNaN((new Date(d)).getTime())){
        document.getElementById('birthday').className = 'form-control';
        valid = true;
    } else {
        document.getElementById('birthday').className = 'form-control invalid';
        valid = false;
    }
    
    return valid;
}

function formvalid() {
    'use strict';
    var arr = [],
        valid = true,
        i = 0;
    arr.push(strValidate('surname', /^\S{1,}$/));
    arr.push(strValidate('name', /^\S{1,}$/));
    arr.push(strValidate('patronymic', /^\S{1,}$/));
    arr.push(isValidDate());
    arr.push(sexvalidate());
    arr.push(strValidate('number', /^37529[\d]{7}$/));
    arr.push(emailvalid());
    arr.push(strValidate('passport', /^([A-Z]{2})([\d]{7})$/));
    arr.push(strValidate('address', /^([^\s]{1,})([А-Яа-я0-9\s\.\/\,]{1,})$/));
    arr.push(strValidate('position', /^([^\s]{1,})([А-Яа-я0-9\s\.\/\,]{1,})$/));
    arr.push(strValidate('experience', /^([\d]{1,}[\,\.]{0,1}[\d]{0,1})$/));
    for (i = 0; i < arr.length; i++) {
        if (arr[i] === false) {
            valid = false;
            break;
        }
    }
    return valid;
}