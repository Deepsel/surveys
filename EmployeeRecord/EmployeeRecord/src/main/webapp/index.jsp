<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Вход в личный кабинет</title>
<base href="${pageContext.request.contextPath}/">
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/signin.css" rel="stylesheet">

<script type="text/javascript" src="resources/js/login.js"></script>

</head>
<body>
	<div class="container">

		<form  class="form-signin" action ="tab/login" method = "post">
			<h2 class="form-signin-heading">Личный кабинет</h2>
			<label for="inputEmail" class="sr-only">Email адрес</label> 
			<input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email адрес" value="${email}" required autofocus> 
			<label for="inputPassword" class="sr-only">Пароль</label> 
			<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Пароль" required>
			
			<input class="btn btn-lg btn-primary btn-block" type="submit" value="Войти" onclick="return loginvalid();">
			<c:if test="${error}">
              <p class="text-danger text-center">Неверный логин или пароль</p>
            </c:if>
		</form>

	</div>
</body>
</html>