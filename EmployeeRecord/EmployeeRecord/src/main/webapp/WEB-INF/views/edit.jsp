<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Личные данные</title>

    <base href="${pageContext.request.contextPath}/">
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="resources/css/cabinet.css" rel="stylesheet">
    <script src="resources/js/script.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
</head>


<body>
    <nav class="navbar navbar-light bg-light justify-content-between">
        <a class="navbar-brand"><img src="resources/images/logo.png" alt=""></a>
        <div class="nav navbar-nav navbar-right">
            <a class="btn btn-outline-primary my-2 my-sm-0" href="tab/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Выйти</a>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block sidebar">
            <c:choose>
		<c:when test="${authenticated.role == 'ADMIN'}">
			<ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="tab/admin?id=${authenticated.id}">Профиль</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/all">Сотрудники</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/addemployee">Добавить сотрудника</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/alldepartment">Отделы</a>
                    </li>
                </ul>
		</c:when>
		<c:otherwise>
			<ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="tab/employee?id=${authenticated.id}">Профиль</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="tab/all">Сотрудники</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Отделы</a>
                    </li>
                    
                </ul>
		</c:otherwise>
	</c:choose>
                
            </nav>

            <div class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
                <div class="col-md-10">
                    <form class="form-signin" role="form" autocomplete="off" action="tab/edit" method="post">
                    <input type="hidden" name="id" value="${authenticated.id}">
                    <input type="hidden" name="role" value="${authenticated.role}">
                        <h2 class="form-signin-heading">Личные данные</h2>
                        <c:if test="${error}">
                            <p class="text-danger">Произошла ошибка.</p>
                        </c:if>
                        <c:if test="${dberror}">
                            <p class="text-danger">Такой Email уже существует.</p>
                        </c:if>
                        
                        <div class="form-group">
                            <label>Фамилия</label>
                            <input name="surname" id="surname" type="text" class="form-control" value="${authenticated.surname}">
                            <label>Имя</label>
                            <input name="name" id="name" type="text" class="form-control" value="${authenticated.name}">
                            <label>Отчество</label>
                            <input id="patronymic" name="patronymic" type="text" class="form-control" value="${authenticated.patronymic}">
                        </div>

                        <div class="form-group">
                            <label>Дата рождения</label>
                            <input name="birthday" id="birthday" type="date" class="form-control" value="${authenticated.birthday}">
                            <label>Пол</label><br>
                            
                           <c:choose>
						  <c:when test="${authenticated.sex == 'MALE'}">
							  <label class="custom-control custom-radio">
	                          <input id="male" name="sex" type="radio" value="MALE" class="custom-control-input" checked>
	                          <span id="sexm" class="custom-control-indicator"></span>
	                          <span class="custom-control-description">Муж.</span>
                        </label>
	                          <label class="custom-control custom-radio">
	                          <input id="female" name="sex" type="radio" value="FEMALE" class="custom-control-input">
	                          <span id="sexf" class="custom-control-indicator"></span>
	                          <span class="custom-control-description">Жен.</span>
                        </label>
						  </c:when>
						  <c:otherwise>
						   <label class="custom-control custom-radio">
							   <input id="male" name="sex" type="radio" value="MALE" class="custom-control-input">
							   <span id="sexm" class="custom-control-indicator"></span>
							   <span class="custom-control-description">Муж.</span>
						   </label>
						   <label class="custom-control custom-radio">
							   <input id="female" name="sex" type="radio" value="FEMALE" class="custom-control-input"checked>
							   <span id="sexf" class="custom-control-indicator"></span>
							   <span class="custom-control-description">Жен.</span>
						   </label>
						  </c:otherwise>
						</c:choose>
                            
                            <br>
                        </div>

                        <div class="form-group">
                            <label>Телефонный номер</label>
                            <input name="number" id="number" type="text" class="form-control" value="${authenticated.number}">
                            <label>Email</label>
                            <input name="email" id="email" type="text" class="form-control" value="${authenticated.email}">
                            <label>Серия и номер паспорта</label>
                            <input name="passport" id="passport" type="text" class="form-control" value="${authenticated.passport}">
                            <label>Адрес проживания</label>
                            <input name="address" id="address" type="text" class="form-control" value="${authenticated.address}">
                        </div>
                        <c:choose>
						  <c:when test="${authenticated.role == 'ADMIN'}">
							    <div class="form-group">
	                            <label>Отдел</label><br>
	                            <select id="department" name="department" class="custom-select">
	                            <c:forEach items="${departments}" var="department">
	                              <option value="${department.id}">${department.name}</option>
	                              </c:forEach>
	                            </select>
	                            <br>
	                            <label>Должность</label>
	                            <input name="position" id="position" type="text" class="form-control" value="${authenticated.position}">
	                            
	                            <label>Стаж(лет)</label>
	                            <fmt:formatNumber var="exp" minIntegerDigits="1" maxFractionDigits="1" value="${authenticated.experiece/12}" />
	                            <input name="experience" id="experience" type="text" class="form-control" value="${exp}">
	
	                        </div>
  						</c:when>
  
					  <c:otherwise>
					    <input type="hidden" name="department" id="department" value="${authenticated.department.id}">
					    <input type="hidden" name="position" id="position" value="${authenticated.position}">
					    <fmt:formatNumber var="exp" minIntegerDigits="1" maxFractionDigits="1" value="${authenticated.experiece/12}" />
					    <input type="hidden" name="experience" id="experience" value="${exp}">
					  </c:otherwise>
					</c:choose>
                        
                        <div class="center">
                            <input class="btn btn-primary btn-lg" type="submit" onclick="return formvalid();" value="Сохранить">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
           
        </div>
    </footer>

</body>

</html>