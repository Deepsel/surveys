import {User} from "../models/User";

export class UserService {

  users: User[] = [{name: 'Roman', email: 'roman@email.com'}];
  selectedUser: User;

  updatelist(user: User) {
    this.users.push(user);
  }

  selectUser(user: User) {
    this.selectedUser = user;
  }

  deleteUser(user: User){
    this.users.splice(this.users.indexOf(user),1)
    this.selectedUser = null;
  }


}
