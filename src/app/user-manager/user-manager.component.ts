import {Component, OnInit} from '@angular/core';
import {User} from "../models/User";
import {UserService} from "./user.service";

@Component({
  selector: 'app-user-manager',
  templateUrl: './user-manager.component.html',
  styleUrls: ['./user-manager.component.css'],
  providers: [UserService]
})
export class UserManagerComponent implements OnInit {


  user: User;
  users: User[];
  constructor(public userService: UserService) {

  }

  ngOnInit() {
    this.users = this.userService.users;
  }


}
