import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {User} from "../models/User";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  user:User;
  userName: string;
  userEmail: string;
  @Output() createUserEvent = new EventEmitter<User>();
  constructor() { }

  ngOnInit() {
  }

  createUser(){
    this.user = new User(this.userName, this.userEmail);
    this.createUserEvent.emit(this.user);

  }

}
