import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../models/User";

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  @Input() user:User;
  @Output() deleteEvent = new EventEmitter<User>();
  constructor() { }

  ngOnInit() {
  }

  deleteUser(user: User){
    this.deleteEvent.emit(user);
  }

}
