import { Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'child-comp',
  template: `<button class="btn btn-success" (click)="change(true)">+</button>
                 <button class="btn btn-danger" (click)="change(false)">-</button>`
})
export class ChildComponent{

  @Output() onChanged = new EventEmitter<boolean>();
  change(increased:any) {
    this.onChanged.emit(increased);
  }
}
