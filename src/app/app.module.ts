import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AppComponent }   from './app.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserManagerComponent } from './user-manager/user-manager.component';
import { UserDetailComponent } from './user-detail/user-detail.component';

@NgModule({
  imports:      [ BrowserModule, FormsModule ],
  declarations: [ AppComponent, UserFormComponent, UserManagerComponent, UserDetailComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
